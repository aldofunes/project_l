# Project L

Project L is a system that helps law firms manage day-to-day workflows and business operations within their law practice. This type of law firm software helps manage a law firm’s cases, contacts, calendars documents, tasks, time tracking, billing, payments, accounting, and more.
