import { MenuIcon } from '@heroicons/react/outline';
import React, { useEffect, useState } from 'react';
import { Route, Switch } from 'react-router-dom';
import Auth from './Auth';
import Clients from './Clients';
import OrganizationPicker from './OrganizationPicker';
import { useFindOrganizationsLazyQuery } from './generated/graphql';
import Home from './Home';
import Users from './Users';
import Nav from './Nav';
import NotFound from './NotFound';
import { session } from './setup/session';
import Spinner from './Spinner';

export default function App(): JSX.Element {
  const [authToken, setAuthToken] = useState(session.getAccessToken());
  const [organizationId, setOrganizationId] = useState(
    localStorage.getItem('current_organization_id'),
  );
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [findOrganizations, { data, error, loading }] = useFindOrganizationsLazyQuery();

  useEffect(() => {
    // subscribe to token updates
    session.on('updated', (token) => setAuthToken(token));

    if (authToken) findOrganizations();

    // unsubscribe when unmounting
    return () => {
      session.removeAllListeners();
    };
  }, [authToken, findOrganizations]);

  if (!authToken) return <Auth />;

  if (loading) return <Spinner />;
  if (error) return <div>error! {error?.message}</div>;
  if (!organizationId || !data?.organizations.map((f) => f.id).includes(organizationId)) {
    return <OrganizationPicker onChange={setOrganizationId} />;
  }

  return (
    <>
      <Nav sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
      <div className="md:pl-64 h-full flex-1">
        <div className="sticky top-0 z-10 md:hidden pl-1 pt-1 sm:pl-3 sm:pt-3 bg-gray-100">
          <button
            type="button"
            className="-ml-0.5 -mt-0.5 h-12 w-12 inline-flex items-center justify-center rounded-md text-gray-500 hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500"
            onClick={() => setSidebarOpen(true)}
          >
            <span className="sr-only">Open sidebar</span>
            <MenuIcon className="h-6 w-6" aria-hidden="true" />
          </button>
        </div>
        <Switch>
          <Route path="/users">
            <Users organizationId={organizationId} />
          </Route>
          <Route path="/clients">
            <Clients organizationId={organizationId} />
          </Route>
          <Route path="/" exact>
            <Home />
          </Route>
          <NotFound />
        </Switch>
      </div>
    </>
  );
}
