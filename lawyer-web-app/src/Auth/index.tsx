import React, { useEffect, useState } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { useRefreshTokenMutation } from '../generated/graphql';
import { session } from '../setup/session';
import Spinner from '../Spinner';
import SignIn from './SignIn';
import SignUp from './SignUp';

export default function Auth(): JSX.Element {
  const [hideAuth, setHideAuth] = useState(true);

  const [refreshToken, { loading }] = useRefreshTokenMutation({
    onCompleted(data) {
      session.setAccessToken(data?.refreshToken);
      console.log('token', session.getAccessToken());
    },
    onError(err) {
      if (
        err.graphQLErrors.find((graphQLError) => graphQLError.extensions.code === 'UNAUTHORIZED')
      ) {
        setHideAuth(false);
      }
    },
  });

  useEffect(() => {
    if (!session.getAccessToken()) {
      refreshToken().catch(console.error);
    }
  }, [refreshToken]);

  if (hideAuth || loading) {
    return <Spinner />;
  }

  return (
    <Switch>
      <Route path="/sign-in" component={SignIn} />
      <Route path="/sign-up" component={SignUp} />
      <Redirect to="/sign-in" />
    </Switch>
  );
}
