import { ApolloClient, from, HttpLink, InMemoryCache } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { onError } from '@apollo/client/link/error';
import { RetryLink } from '@apollo/client/link/retry';
import { loader } from 'graphql.macro';
import jwt, { JwtPayload } from 'jsonwebtoken';
import { session } from './session';

const REFRESH_TOKEN = loader('../mutations/refresh-token.graphql');

const withToken = setContext(async (request, previousContext) => {
  const accessToken = session.getAccessToken();

  if (typeof accessToken === 'string') {
    const decoded = jwt.decode(accessToken) as JwtPayload;
    if (typeof decoded?.exp === 'number') {
      const expirationDate = new Date(decoded.exp * 1000);

      if (Date.now() < expirationDate.getTime()) {
        return { headers: { authorization: `Bearer ${accessToken}` } };
      }
    }
  }

  if (!['SignIn', 'RefreshToken', 'SignUp'].includes(String(request.operationName))) {
    try {
      const { data } = await client.mutate({ mutation: REFRESH_TOKEN });
      if (data.refreshToken.accessToken) {
        session.setAccessToken(data.refreshToken.accessToken);
        return {
          headers: { authorization: `Bearer ${data.refreshToken.accessToken}` },
        };
      }
    } catch (err) {
      console.error(err);
    }
  }
});

const refreshToken = onError(({ networkError, graphQLErrors, operation }) => {
  console.error({ networkError, graphQLErrors, operation });
});

const retryLink = new RetryLink({
  attempts: {
    retryIf: (error, operation) =>
      !['SignIn', 'RefreshToken', 'SignUp'].includes(operation.operationName),
  },
});

const httpLink = new HttpLink({
  uri: 'http://localhost:8000/graphql',
  credentials: 'include',
});

const link = from([withToken, refreshToken, retryLink, httpLink]);

export const client = new ApolloClient({
  link,
  cache: new InMemoryCache(),
});
