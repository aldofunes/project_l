import { EventEmitter } from 'events';

class Session extends EventEmitter {
  private accessToken?: string;

  getAccessToken(): string | undefined {
    return this.accessToken;
  }

  setAccessToken(token?: string) {
    this.accessToken = token;
    this.emit('updated', token);
  }
}

export const session = new Session();
