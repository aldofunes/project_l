import React from 'react';
import { NavLink, useParams, useRouteMatch } from 'react-router-dom';
import { useGetClientQuery } from '../generated/graphql';
import Spinner from '../Spinner';

interface Props {
  organizationId: string;
}
export default function ClientDetails({ organizationId }: Props): JSX.Element {
  const { url } = useRouteMatch();
  const tabs = [
    { name: 'Contacts', to: `${url}`, exact: true },
    { name: 'Documents', to: `${url}/documents`, exact: false },
    { name: 'InterviewTests', to: `${url}/tests`, exact: false },
  ];

  const { clientId } = useParams<{ clientId: string }>();

  const { data, error, loading } = useGetClientQuery({ variables: { id: clientId } });

  if (loading) return <Spinner />;
  if (error) return <div>error! {error?.message}</div>;
  if (!data) return <div>error! no data</div>;

  return <>Hello {data.client.name}</>;
}
