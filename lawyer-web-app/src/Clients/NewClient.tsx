import { gql } from '@apollo/client';
import { ChevronRightIcon } from '@heroicons/react/solid';
import React, { SyntheticEvent, useCallback, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useRegisterClientMutation } from '../generated/graphql';

interface Props {
  organizationId: string;
}

export default function NewClient({ organizationId }: Props): JSX.Element {
  const [name, setName] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [websiteUrl, setWebsiteUrl] = useState('');
  const [streetAddress, setstreetAddress] = useState('');
  const [city, setCity] = useState('');
  const [state, setState] = useState('');
  const [postalCode, setPostalCode] = useState('');
  const [countryCode, setCountryCode] = useState('');

  const history = useHistory();

  const [registerClient, { error }] = useRegisterClientMutation({
    onCompleted: () => {
      history.push('/clients');
    },
    onError: console.error,
    update(cache, { data }) {
      cache.modify({
        fields: {
          findClients(existingClients = []) {
            if (data?.client) {
              const newClientRef = cache.writeFragment({
                data: data.client,
                fragment: gql`
                  fragment NewClient on Client {
                    id
                  }
                `,
              });
              return [...existingClients, newClientRef];
            }
          },
        },
      });
    },
  });

  const onSubmit = useCallback(
    (event: SyntheticEvent) => {
      event.preventDefault();

      registerClient({
        variables: {
          organizationId,
          name,
          phoneNumber,
          websiteUrl,
          streetAddress,
          city,
          state,
          postalCode,
          countryCode,
        },
      });
    },

    [
      registerClient,
      organizationId,
      name,
      phoneNumber,
      websiteUrl,
      streetAddress,
      city,
      state,
      postalCode,
      countryCode,
    ],
  );

  return (
    <form onSubmit={onSubmit} className="space-y-8 divide-y divide-gray-200">
      {error && (
        <div>
          <p>Error: {error.message}</p>
        </div>
      )}
      <div className="space-y-8 divide-y divide-gray-200">
        <div className="pt-8">
          <div>
            <h3 className="text-lg leading-6 font-medium">Client Information</h3>
            <p className="mt-1 text-sm text-gray-500 dark:text-gray-400">
              Use a permanent address where they can receive mail.
            </p>
          </div>
          <div className="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
            <div className="sm:col-span-6">
              <label
                htmlFor="name"
                className="block text-sm font-medium text-gray-700 dark:text-gray-300"
              >
                Name
              </label>
              <div className="mt-1">
                <input
                  id="name"
                  value={name}
                  onChange={(event) => setName(event.target.value)}
                  name="name"
                  type="text"
                  autoComplete="organization"
                  className="shadow-sm bg-transparent focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                />
              </div>
            </div>
            <div className="sm:col-span-3">
              <label
                htmlFor="name"
                className="block text-sm font-medium text-gray-700 dark:text-gray-300"
              >
                Phone Number
              </label>
              <div className="mt-1">
                <input
                  id="phoneNumber"
                  value={phoneNumber}
                  onChange={(event) => setPhoneNumber(event.target.value)}
                  name="phoneNumber"
                  type="tel"
                  autoComplete="tel"
                  className="shadow-sm bg-transparent focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                />
              </div>
            </div>
            <div className="sm:col-span-3">
              <label
                htmlFor="name"
                className="block text-sm font-medium text-gray-700 dark:text-gray-300"
              >
                Website URL
              </label>
              <div className="mt-1">
                <input
                  id="websiteUrl"
                  value={websiteUrl}
                  onChange={(event) => setWebsiteUrl(event.target.value)}
                  name="websiteUrl"
                  type="url"
                  autoComplete="url"
                  className="shadow-sm bg-transparent focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                />
              </div>
            </div>

            <div className="sm:col-span-6">
              <label
                htmlFor="streetAddress"
                className="block text-sm font-medium text-gray-700 dark:text-gray-300"
              >
                Street address
              </label>
              <div className="mt-1">
                <input
                  type="text"
                  name="streetAddress"
                  id="streetAddress"
                  value={streetAddress}
                  onChange={(event) => setstreetAddress(event.target.value)}
                  autoComplete="street-address"
                  className="shadow-sm bg-transparent focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                />
              </div>
            </div>

            <div className="sm:col-span-2">
              <label
                htmlFor="city"
                className="block text-sm font-medium text-gray-700 dark:text-gray-300"
              >
                City
              </label>
              <div className="mt-1">
                <input
                  type="text"
                  name="city"
                  id="city"
                  value={city}
                  onChange={(event) => setCity(event.target.value)}
                  autoComplete="address-level2"
                  className="shadow-sm bg-transparent focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                />
              </div>
            </div>

            <div className="sm:col-span-2">
              <label
                htmlFor="state"
                className="block text-sm font-medium text-gray-700 dark:text-gray-300"
              >
                State / Province
              </label>
              <div className="mt-1">
                <input
                  type="text"
                  name="state"
                  id="state"
                  value={state}
                  onChange={(event) => setState(event.target.value)}
                  autoComplete="address-level1"
                  className="shadow-sm bg-transparent focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                />
              </div>
            </div>

            <div className="sm:col-span-2">
              <label
                htmlFor="postalCode"
                className="block text-sm font-medium text-gray-700 dark:text-gray-300"
              >
                ZIP / Postal code
              </label>
              <div className="mt-1">
                <input
                  type="text"
                  name="postalCode"
                  id="postalCode"
                  value={postalCode}
                  onChange={(event) => setPostalCode(event.target.value)}
                  autoComplete="postal-code"
                  className="shadow-sm bg-transparent focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                />
              </div>
            </div>

            <div className="sm:col-span-2">
              <label
                htmlFor="postal-code"
                className="block text-sm font-medium text-gray-700 dark:text-gray-300"
              >
                Country
              </label>
              <div className="mt-1">
                <input
                  id="countryCode"
                  value={countryCode}
                  onChange={(event) => setCountryCode(event.target.value)}
                  name="countryCode"
                  type="text"
                  autoComplete="country"
                  className="shadow-sm bg-transparent focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="pt-5">
        <div className="flex justify-end">
          <button
            type="button"
            className="bg-white dark:bg-gray-700 py-2 px-4 border border-gray-300 dark:border-gray-600 rounded-md shadow-sm text-sm font-medium text-gray-700 dark:text-gray-300 hover:bg-gray-50 dark:hover:bg-gray-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            Cancel
          </button>
          <button
            type="submit"
            className="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            Save
          </button>
        </div>
      </div>
    </form>
  );
}

export function Breadcrumb(): JSX.Element {
  return (
    <nav className="hidden sm:flex" aria-label="Breadcrumb">
      <ol className="flex items-center space-x-4">
        <li>
          <div className="flex">
            <Link
              to="/clients"
              className="text-sm font-medium text-gray-600 hover dark:text-gray-400 hover:dark:text-gray-200"
            >
              Clients
            </Link>
          </div>
        </li>
        <li>
          <div className="flex items-center">
            <ChevronRightIcon className="flex-shrink-0 h-5 w-5 text-gray-500" aria-hidden="true" />
            <Link
              to="/clients/new"
              className="ml-4 text-sm font-medium text-gray-600 hover dark:text-gray-400 hover:dark:text-gray-200"
            >
              New Client
            </Link>
          </div>
        </li>
      </ol>
    </nav>
  );
}
