import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import ClientDetails from './ClientDetails';
import ClientList from './ClientList';
import EditClient from './EditClient';
import NewClient from './NewClient';

interface Props {
  organizationId: string;
}

export default function Clients({ organizationId }: Props): JSX.Element {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <Route path={`${path}/new`}>
        <NewClient organizationId={organizationId} />
      </Route>
      <Route path={`${path}/:clientId/edit`}>
        <EditClient organizationId={organizationId} />
      </Route>
      <Route path={`${path}/:clientId`}>
        <ClientDetails organizationId={organizationId} />
      </Route>
      <Route path={`${path}`}>
        <ClientList organizationId={organizationId} />
      </Route>
    </Switch>
  );
}
