import React from 'react';
import { Link } from 'react-router-dom';
import EmptyList from '../EmptyList';
import { useFindClientsQuery } from '../generated/graphql';
import Spinner from '../Spinner';

interface Props {
  organizationId: string;
}

export default function ClientList({ organizationId }: Props): JSX.Element {
  const { data, loading, error } = useFindClientsQuery({
    variables: { organizationId },
  });

  if (loading) return <Spinner />;
  if (error) return <div>error! {error?.message}</div>;
  if (!data) return <div>error! no data</div>;

  if (!data?.clients.length) {
    return (
      <EmptyList
        title="No clients"
        description="Get started by creating a new client."
        callToAction={{ text: 'New Client', to: '/clients/new' }}
      />
    );
  }

  return (
    <>
      <div className="flex-1 relative z-0 flex overflow-hidden h-full">
        <main className="flex-1 relative z-0 overflow-y-auto focus:outline-none xl:order-last">
          {/* Start main area*/}
          <div className="absolute inset-0 py-6 px-4 sm:px-6 lg:px-8">
            <div className="">
              <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                  <div className="shadow overflow-hidden border-b border-gray-200 dark:border-gray-700 sm:rounded-lg">
                    <table className="min-w-full divide-y divide-gray-200 dark:divide-gray-700">
                      <thead className="bg-gray-50 dark:bg-gray-700">
                        <tr>
                          <th
                            scope="col"
                            className="px-6 py-3 text-left text-xs font-medium uppercase tracking-wider"
                          >
                            Name
                          </th>
                          <th scope="col" className="relative px-6 py-3">
                            <span className="sr-only">Edit</span>
                          </th>
                        </tr>
                      </thead>
                      <tbody className="bg-white dark:bg-gray-800 divide-y divide-gray-200 dark:divide-gray-700">
                        {data.clients.map((client) => (
                          <tr key={client.id}>
                            <td className="px-6 py-4 whitespace-nowrap">
                              <div className="flex items-center">
                                <div className="ml-4">
                                  <div className="text-sm font-medium">
                                    <Link
                                      className="text-indigo-600 hover:text-indigo-900 dark:text-indigo-100 dark:hover:text-indigo-300"
                                      to={`/clients/${client.id}`}
                                    >
                                      {client.name}
                                    </Link>
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                              <Link
                                to={`/clients/${client.id}/edit`}
                                className="text-indigo-600 hover:text-indigo-900 dark:text-indigo-100 dark:hover:text-indigo-300"
                              >
                                Edit
                              </Link>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* End main area */}
        </main>
        <aside className="hidden relative xl:order-first xl:flex xl:flex-col flex-shrink-0 w-96 border-r border-gray-200 overflow-y-auto">
          {/* Start secondary column (hidden on smaller screens) */}
          <div className="absolute inset-0 py-6 px-4 sm:px-6 lg:px-8">
            <div className="h-full border-2 border-gray-200 border-dashed rounded-lg" />
          </div>
          {/* End secondary column */}
        </aside>
      </div>
    </>
  );
}
