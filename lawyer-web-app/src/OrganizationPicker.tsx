import { CheckIcon } from '@heroicons/react/solid';
import React, { useEffect } from 'react';
import { useFindOrganizationsQuery } from './generated/graphql';
import Spinner from './Spinner';

interface Props {
  onChange: (organizationId: string) => void;
}

export default function OrganizationPicker({ onChange }: Props): JSX.Element {
  const { data, error, loading } = useFindOrganizationsQuery();

  useEffect(() => {
    localStorage.removeItem('current_organization_id');
  }, []);

  if (loading) return <Spinner />;
  if (error) return <div>error! {error?.message}</div>;

  const onClick = (organizationId: string) => {
    localStorage.setItem('current_organization_id', organizationId);
    onChange(organizationId);
  };

  return (
    <div className="min-h-full flex flex-col justify-center py-12 sm:px-6 lg:px-8 bg">
      <div className="sm:mx-auto sm:w-full sm:max-w-md">
        <img
          className="mx-auto h-12 w-auto"
          src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg"
          alt="Workflow"
        />
        <h2 className="mt-6 text-center text-3xl font-extrabold">Select an Organization</h2>
      </div>

      <ul className="flex justify-center flex-wrap">
        {data?.organizations.map((organization) => (
          <li
            key={organization.id}
            className="flex flex-col text-center bg-white dark:bg-gray-800 rounded-lg shadow divide-y divide-gray-200 m-2 w-12"
          >
            <div className="flex-1 flex flex-col p-8">
              {organization.logoUrl && (
                <img
                  className="w-32 h-32 flex-shrink-0 mx-auto rounded-full"
                  src={organization.logoUrl}
                  alt=""
                />
              )}
              <h3 className="mt-6  text-sm font-medium">{organization.slug}</h3>
              <dl className="mt-1 flex-grow flex flex-col justify-between">
                <dt className="sr-only">Title</dt>
                <dd className="text-sm">{organization.streetAddress}</dd>
                <dt className="sr-only">Role</dt>
                <dd className="mt-3">
                  <span className="px-2 py-1text-xs font-medium">{organization.name}</span>
                </dd>
              </dl>
            </div>
            <div className="-mt-px flex divide-x divide-gray-200">
              <div className="w-0 flex-1 flex p-8">
                <button
                  onClick={() => onClick(organization.id)}
                  className="relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm font-medium border border-transparent rounded-bl-lg hover:text-gray-500 dark:hover:text-gray-300"
                >
                  <CheckIcon className="w-5 h-5" aria-hidden="true" />
                  <span className="ml-3">Select Organization</span>
                </button>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}
