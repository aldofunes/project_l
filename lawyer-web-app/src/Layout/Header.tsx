import React from 'react';

interface Props {
  children: React.ReactNode;
  title: string;
}

export default function Header({ children, title }: Props): JSX.Element {
  return (
    <header className="mx-auto py-6 px-4 sm:px-6 lg:px-8 flex justify-between h-full">
      <h1 className="text-3xl font-bold text-gray-900 dark:text-gray-100">{title}</h1>
      {children}
    </header>
  );
}
