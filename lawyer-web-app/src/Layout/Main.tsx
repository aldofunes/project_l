import React from 'react';

interface Props {
  children: React.ReactNode;
}

export default function Main({ children }: Props): JSX.Element {
  return (
    <main className="flex-1 py-6">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">{children}</div>
    </main>
  );
}
