import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /**
   * ISO 8601 calendar date without timezone.
   * Format: %Y-%m-%d
   *
   * # Examples
   *
   * * `1994-11-13`
   * * `2000-02-24`
   */
  NaiveDate: any;
  /**
   * ISO 8601 combined date and time without timezone.
   *
   * # Examples
   *
   * * `2015-07-01T08:59:60.123`,
   */
  NaiveDateTime: any;
  /**
   * A UUID is a unique 128-bit number, stored as 16 octets. UUIDs are parsed as Strings
   * within GraphQL. UUIDs are used to assign unique identifiers to entities without requiring a central
   * allocating authority.
   *
   * # References
   *
   * * [Wikipedia: Universally Unique Identifier](http://en.wikipedia.org/wiki/Universally_unique_identifier)
   * * [RFC4122: A Universally Unique IDentifier (UUID) URN Namespace](http://tools.ietf.org/html/rfc4122)
   */
  UUID: any;
};

export type Client = {
  __typename?: 'Client';
  city?: Maybe<Scalars['String']>;
  contacts: Array<Contact>;
  countryCode?: Maybe<Scalars['String']>;
  id: Scalars['UUID'];
  name?: Maybe<Scalars['String']>;
  organizationId: Scalars['UUID'];
  phoneNumber?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  registeredAt: Scalars['NaiveDateTime'];
  state?: Maybe<Scalars['String']>;
  streetAddress?: Maybe<Scalars['String']>;
  websiteUrl?: Maybe<Scalars['String']>;
};

/**
 * Represents a reachable contact
 *
 * # Example
 *
 * ```
 * let contact = Contact {
 * id: Uuid::new_v4(),
 * client_id: Uuid::new_v4(),
 * name: String::from("John Doe"),
 * email: Some(String::from("john.doe@example.com")),
 * phone_calling_code: Some(String::from("+52")),
 * phone_country_code: Some(String::from("MEX")),
 * phone: Some(String::From("5522889933")),
 * registered_at: Utc::now().naive_utc(),
 * };
 * ```
 */
export type Contact = {
  __typename?: 'Contact';
  clientId: Scalars['UUID'];
  /** the contact's email */
  email?: Maybe<Scalars['String']>;
  id: Scalars['UUID'];
  /** the contact's full name */
  name: Scalars['String'];
  /** the contact's phone number */
  phoneNumber?: Maybe<Scalars['String']>;
  registeredAt: Scalars['NaiveDateTime'];
};

export type Organization = {
  __typename?: 'Organization';
  /** the annual revenue range of the organization */
  annualRevenueRange?: Maybe<Scalars['String']>;
  /** registered address */
  city?: Maybe<Scalars['String']>;
  /** registered address country code in ISO 3 */
  countryCode?: Maybe<Scalars['String']>;
  /** amount of employees the organization has */
  employees?: Maybe<Scalars['Int']>;
  /** date at which the organization was registered */
  foundedAt?: Maybe<Scalars['NaiveDate']>;
  /** system-generated uuid that uniquely identifies the organization */
  id: Scalars['UUID'];
  /** url to the image of the organization's logo */
  logoUrl?: Maybe<Scalars['String']>;
  /** full name of the organization */
  name: Scalars['String'];
  /** phone number where the organization can be reached by customers */
  phoneNumber?: Maybe<Scalars['String']>;
  /** registered address */
  postalCode?: Maybe<Scalars['String']>;
  /** date and time of registration into the system */
  registeredAt: Scalars['NaiveDateTime'];
  /** url-friendly unique identifier for a organization */
  slug: Scalars['String'];
  /** registered address */
  state?: Maybe<Scalars['String']>;
  /** registered address */
  streetAddress?: Maybe<Scalars['String']>;
  /** url of the organization's website */
  websiteUrl?: Maybe<Scalars['String']>;
};

export type PublicOrganization = {
  __typename?: 'PublicOrganization';
  /** system-generated uuid that uniquely identifies the organization */
  id: Scalars['UUID'];
  /** full name of the organization */
  name: Scalars['String'];
  /** url-friendly unique identifier for a organization */
  slug: Scalars['String'];
};

export type RootMutation = {
  __typename?: 'RootMutation';
  deleteClient: Scalars['Boolean'];
  deleteUser: Scalars['Boolean'];
  refreshToken: Scalars['String'];
  registerClient: Client;
  registerContact: Contact;
  registerOrganization: Organization;
  registerUser: User;
  signIn: Scalars['String'];
  signOut: Scalars['Boolean'];
  signUp: SignUpResponse;
  updateClient: Client;
  updateUser: User;
};


export type RootMutationDeleteClientArgs = {
  id: Scalars['UUID'];
};


export type RootMutationDeleteUserArgs = {
  id: Scalars['UUID'];
};


export type RootMutationRegisterClientArgs = {
  city?: Maybe<Scalars['String']>;
  countryCode?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  organizationId: Scalars['UUID'];
  phoneNumber?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  streetAddress?: Maybe<Scalars['String']>;
  websiteUrl?: Maybe<Scalars['String']>;
};


export type RootMutationRegisterContactArgs = {
  clientId: Scalars['UUID'];
  email?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  phoneNumber?: Maybe<Scalars['String']>;
};


export type RootMutationRegisterOrganizationArgs = {
  annualRevenueRange?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  countryCode?: Maybe<Scalars['String']>;
  employees?: Maybe<Scalars['Int']>;
  foundedAt?: Maybe<Scalars['NaiveDate']>;
  logoUrl?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  phoneNumber?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  slug: Scalars['String'];
  state?: Maybe<Scalars['String']>;
  streetAddress?: Maybe<Scalars['String']>;
  websiteUrl?: Maybe<Scalars['String']>;
};


export type RootMutationRegisterUserArgs = {
  email: Scalars['String'];
  firstName?: Maybe<Scalars['String']>;
  language?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  organizationId: Scalars['UUID'];
  password: Scalars['String'];
  timeZone?: Maybe<Scalars['String']>;
  totpSecret?: Maybe<Scalars['String']>;
  username: Scalars['String'];
};


export type RootMutationSignInArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type RootMutationSignUpArgs = {
  email: Scalars['String'];
  firstName: Scalars['String'];
  language?: Maybe<Scalars['String']>;
  lastName: Scalars['String'];
  organizationName: Scalars['String'];
  password: Scalars['String'];
  timeZone?: Maybe<Scalars['String']>;
};


export type RootMutationUpdateClientArgs = {
  city?: Maybe<Scalars['String']>;
  countryCode?: Maybe<Scalars['String']>;
  id: Scalars['UUID'];
  name?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  streetAddress?: Maybe<Scalars['String']>;
  websiteUrl?: Maybe<Scalars['String']>;
};


export type RootMutationUpdateUserArgs = {
  email?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  id: Scalars['UUID'];
  language?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  timeZone?: Maybe<Scalars['String']>;
  totpSecret?: Maybe<Scalars['String']>;
  username?: Maybe<Scalars['String']>;
};

export type RootQuery = {
  __typename?: 'RootQuery';
  findClients: Array<Client>;
  findContacts: Array<Contact>;
  findOrganizations: Array<Organization>;
  findUsers: Array<User>;
  getClient: Client;
  getContact: Contact;
  getOrganization: Organization;
  getOrganizationBySlug: Organization;
  getPublicOrganization: PublicOrganization;
  getUser: User;
};


export type RootQueryFindClientsArgs = {
  organizationId: Scalars['UUID'];
};


export type RootQueryFindContactsArgs = {
  clientId: Scalars['UUID'];
};


export type RootQueryFindUsersArgs = {
  organizationId: Scalars['UUID'];
};


export type RootQueryGetClientArgs = {
  id: Scalars['UUID'];
};


export type RootQueryGetContactArgs = {
  id: Scalars['UUID'];
};


export type RootQueryGetOrganizationArgs = {
  id: Scalars['UUID'];
};


export type RootQueryGetOrganizationBySlugArgs = {
  slug: Scalars['String'];
};


export type RootQueryGetPublicOrganizationArgs = {
  slug: Scalars['String'];
};


export type RootQueryGetUserArgs = {
  id: Scalars['UUID'];
};

export type SignUpResponse = {
  __typename?: 'SignUpResponse';
  accessToken: Scalars['String'];
  organization: Organization;
  user: User;
};

export type User = {
  __typename?: 'User';
  email: Scalars['String'];
  firstName?: Maybe<Scalars['String']>;
  id: Scalars['UUID'];
  language?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  registeredAt: Scalars['NaiveDateTime'];
  timeZone?: Maybe<Scalars['String']>;
  totpSecret?: Maybe<Scalars['String']>;
  username: Scalars['String'];
};

export type DeleteClientMutationVariables = Exact<{
  id: Scalars['UUID'];
}>;


export type DeleteClientMutation = { __typename?: 'RootMutation', deleteClient: boolean };

export type DeleteUserMutationVariables = Exact<{
  id: Scalars['UUID'];
}>;


export type DeleteUserMutation = { __typename?: 'RootMutation', deleteUser: boolean };

export type RefreshTokenMutationVariables = Exact<{ [key: string]: never; }>;


export type RefreshTokenMutation = { __typename?: 'RootMutation', refreshToken: string };

export type RegisterClientMutationVariables = Exact<{
  organizationId: Scalars['UUID'];
  name?: Maybe<Scalars['String']>;
  streetAddress?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  countryCode?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['String']>;
  websiteUrl?: Maybe<Scalars['String']>;
}>;


export type RegisterClientMutation = { __typename?: 'RootMutation', client: { __typename?: 'Client', id: any, organizationId: any, name?: string | null | undefined, streetAddress?: string | null | undefined, city?: string | null | undefined, state?: string | null | undefined, postalCode?: string | null | undefined, countryCode?: string | null | undefined, phoneNumber?: string | null | undefined, websiteUrl?: string | null | undefined, registeredAt: any } };

export type RegisterContactMutationVariables = Exact<{
  clientId: Scalars['UUID'];
  name: Scalars['String'];
  email?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['String']>;
}>;


export type RegisterContactMutation = { __typename?: 'RootMutation', contact: { __typename?: 'Contact', id: any, clientId: any, name: string, email?: string | null | undefined, phoneNumber?: string | null | undefined, registeredAt: any } };

export type RegisterOrganizationMutationVariables = Exact<{
  name: Scalars['String'];
  slug: Scalars['String'];
  logoUrl?: Maybe<Scalars['String']>;
  streetAddress?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  countryCode?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['String']>;
  websiteUrl?: Maybe<Scalars['String']>;
  employees?: Maybe<Scalars['Int']>;
  foundedAt?: Maybe<Scalars['NaiveDate']>;
  annualRevenueRange?: Maybe<Scalars['String']>;
}>;


export type RegisterOrganizationMutation = { __typename?: 'RootMutation', organization: { __typename?: 'Organization', id: any, registeredAt: any, name: string, slug: string, logoUrl?: string | null | undefined, streetAddress?: string | null | undefined, city?: string | null | undefined, state?: string | null | undefined, postalCode?: string | null | undefined, countryCode?: string | null | undefined, phoneNumber?: string | null | undefined, websiteUrl?: string | null | undefined, employees?: number | null | undefined, foundedAt?: any | null | undefined, annualRevenueRange?: string | null | undefined } };

export type RegisterUserMutationVariables = Exact<{
  organizationId: Scalars['UUID'];
  username: Scalars['String'];
  email: Scalars['String'];
  password: Scalars['String'];
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  timeZone?: Maybe<Scalars['String']>;
  language?: Maybe<Scalars['String']>;
  totpSecret?: Maybe<Scalars['String']>;
}>;


export type RegisterUserMutation = { __typename?: 'RootMutation', user: { __typename?: 'User', id: any, username: string, email: string, firstName?: string | null | undefined, lastName?: string | null | undefined, timeZone?: string | null | undefined, language?: string | null | undefined, totpSecret?: string | null | undefined, registeredAt: any } };

export type SignInMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type SignInMutation = { __typename?: 'RootMutation', signIn: string };

export type SignOutMutationVariables = Exact<{ [key: string]: never; }>;


export type SignOutMutation = { __typename?: 'RootMutation', signOut: boolean };

export type SignUpMutationVariables = Exact<{
  organizationName: Scalars['String'];
  email: Scalars['String'];
  password: Scalars['String'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
}>;


export type SignUpMutation = { __typename?: 'RootMutation', signUp: { __typename?: 'SignUpResponse', accessToken: string, organization: { __typename?: 'Organization', id: any, slug: string }, user: { __typename?: 'User', id: any, username: string } } };

export type UpdateClientMutationVariables = Exact<{
  id: Scalars['UUID'];
  city?: Maybe<Scalars['String']>;
  countryCode?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  streetAddress?: Maybe<Scalars['String']>;
  websiteUrl?: Maybe<Scalars['String']>;
}>;


export type UpdateClientMutation = { __typename?: 'RootMutation', client: { __typename?: 'Client', id: any, organizationId: any, registeredAt: any, city?: string | null | undefined, countryCode?: string | null | undefined, name?: string | null | undefined, phoneNumber?: string | null | undefined, postalCode?: string | null | undefined, state?: string | null | undefined, streetAddress?: string | null | undefined, websiteUrl?: string | null | undefined } };

export type UpdateUserMutationVariables = Exact<{
  id: Scalars['UUID'];
  username: Scalars['String'];
  email: Scalars['String'];
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  timeZone?: Maybe<Scalars['String']>;
  language?: Maybe<Scalars['String']>;
  totpSecret?: Maybe<Scalars['String']>;
}>;


export type UpdateUserMutation = { __typename?: 'RootMutation', user: { __typename?: 'User', id: any, username: string, email: string, firstName?: string | null | undefined, lastName?: string | null | undefined, timeZone?: string | null | undefined, language?: string | null | undefined, totpSecret?: string | null | undefined, registeredAt: any } };

export type FindClientsQueryVariables = Exact<{
  organizationId: Scalars['UUID'];
}>;


export type FindClientsQuery = { __typename?: 'RootQuery', clients: Array<{ __typename?: 'Client', id: any, organizationId: any, registeredAt: any, name?: string | null | undefined, streetAddress?: string | null | undefined, city?: string | null | undefined, state?: string | null | undefined, postalCode?: string | null | undefined, countryCode?: string | null | undefined, phoneNumber?: string | null | undefined, websiteUrl?: string | null | undefined }> };

export type FindContactsQueryVariables = Exact<{
  clientId: Scalars['UUID'];
}>;


export type FindContactsQuery = { __typename?: 'RootQuery', contacts: Array<{ __typename?: 'Contact', id: any, clientId: any, name: string, email?: string | null | undefined, phoneNumber?: string | null | undefined, registeredAt: any }> };

export type FindOrganizationsQueryVariables = Exact<{ [key: string]: never; }>;


export type FindOrganizationsQuery = { __typename?: 'RootQuery', organizations: Array<{ __typename?: 'Organization', id: any, registeredAt: any, name: string, slug: string, annualRevenueRange?: string | null | undefined, city?: string | null | undefined, countryCode?: string | null | undefined, employees?: number | null | undefined, foundedAt?: any | null | undefined, logoUrl?: string | null | undefined, phoneNumber?: string | null | undefined, postalCode?: string | null | undefined, state?: string | null | undefined, streetAddress?: string | null | undefined, websiteUrl?: string | null | undefined }> };

export type FindUsersQueryVariables = Exact<{
  organizationId: Scalars['UUID'];
}>;


export type FindUsersQuery = { __typename?: 'RootQuery', users: Array<{ __typename?: 'User', id: any, registeredAt: any, username: string, email: string, firstName?: string | null | undefined, lastName?: string | null | undefined, timeZone?: string | null | undefined, language?: string | null | undefined, totpSecret?: string | null | undefined }> };

export type GetClientQueryVariables = Exact<{
  id: Scalars['UUID'];
}>;


export type GetClientQuery = { __typename?: 'RootQuery', client: { __typename?: 'Client', id: any, organizationId: any, registeredAt: any, name?: string | null | undefined, streetAddress?: string | null | undefined, city?: string | null | undefined, state?: string | null | undefined, postalCode?: string | null | undefined, countryCode?: string | null | undefined, phoneNumber?: string | null | undefined, websiteUrl?: string | null | undefined, contacts: Array<{ __typename?: 'Contact', id: any, name: string, email?: string | null | undefined }> } };

export type GetOrganizationQueryVariables = Exact<{
  id: Scalars['UUID'];
}>;


export type GetOrganizationQuery = { __typename?: 'RootQuery', organization: { __typename?: 'Organization', id: any, registeredAt: any, slug: string, name: string, logoUrl?: string | null | undefined, streetAddress?: string | null | undefined, city?: string | null | undefined, state?: string | null | undefined, postalCode?: string | null | undefined, countryCode?: string | null | undefined, phoneNumber?: string | null | undefined, websiteUrl?: string | null | undefined, employees?: number | null | undefined, foundedAt?: any | null | undefined, annualRevenueRange?: string | null | undefined } };

export type GetPublicOrganizationQueryVariables = Exact<{
  slug: Scalars['String'];
}>;


export type GetPublicOrganizationQuery = { __typename?: 'RootQuery', organization: { __typename?: 'PublicOrganization', id: any, slug: string, name: string } };


export const DeleteClientDocument = gql`
    mutation DeleteClient($id: UUID!) {
  deleteClient(id: $id)
}
    `;
export type DeleteClientMutationFn = Apollo.MutationFunction<DeleteClientMutation, DeleteClientMutationVariables>;

/**
 * __useDeleteClientMutation__
 *
 * To run a mutation, you first call `useDeleteClientMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteClientMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteClientMutation, { data, loading, error }] = useDeleteClientMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteClientMutation(baseOptions?: Apollo.MutationHookOptions<DeleteClientMutation, DeleteClientMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteClientMutation, DeleteClientMutationVariables>(DeleteClientDocument, options);
      }
export type DeleteClientMutationHookResult = ReturnType<typeof useDeleteClientMutation>;
export type DeleteClientMutationResult = Apollo.MutationResult<DeleteClientMutation>;
export type DeleteClientMutationOptions = Apollo.BaseMutationOptions<DeleteClientMutation, DeleteClientMutationVariables>;
export const DeleteUserDocument = gql`
    mutation DeleteUser($id: UUID!) {
  deleteUser(id: $id)
}
    `;
export type DeleteUserMutationFn = Apollo.MutationFunction<DeleteUserMutation, DeleteUserMutationVariables>;

/**
 * __useDeleteUserMutation__
 *
 * To run a mutation, you first call `useDeleteUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteUserMutation, { data, loading, error }] = useDeleteUserMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteUserMutation(baseOptions?: Apollo.MutationHookOptions<DeleteUserMutation, DeleteUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteUserMutation, DeleteUserMutationVariables>(DeleteUserDocument, options);
      }
export type DeleteUserMutationHookResult = ReturnType<typeof useDeleteUserMutation>;
export type DeleteUserMutationResult = Apollo.MutationResult<DeleteUserMutation>;
export type DeleteUserMutationOptions = Apollo.BaseMutationOptions<DeleteUserMutation, DeleteUserMutationVariables>;
export const RefreshTokenDocument = gql`
    mutation RefreshToken {
  refreshToken
}
    `;
export type RefreshTokenMutationFn = Apollo.MutationFunction<RefreshTokenMutation, RefreshTokenMutationVariables>;

/**
 * __useRefreshTokenMutation__
 *
 * To run a mutation, you first call `useRefreshTokenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRefreshTokenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [refreshTokenMutation, { data, loading, error }] = useRefreshTokenMutation({
 *   variables: {
 *   },
 * });
 */
export function useRefreshTokenMutation(baseOptions?: Apollo.MutationHookOptions<RefreshTokenMutation, RefreshTokenMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RefreshTokenMutation, RefreshTokenMutationVariables>(RefreshTokenDocument, options);
      }
export type RefreshTokenMutationHookResult = ReturnType<typeof useRefreshTokenMutation>;
export type RefreshTokenMutationResult = Apollo.MutationResult<RefreshTokenMutation>;
export type RefreshTokenMutationOptions = Apollo.BaseMutationOptions<RefreshTokenMutation, RefreshTokenMutationVariables>;
export const RegisterClientDocument = gql`
    mutation RegisterClient($organizationId: UUID!, $name: String, $streetAddress: String, $city: String, $state: String, $postalCode: String, $countryCode: String, $phoneNumber: String, $websiteUrl: String) {
  client: registerClient(
    organizationId: $organizationId
    name: $name
    streetAddress: $streetAddress
    city: $city
    state: $state
    postalCode: $postalCode
    countryCode: $countryCode
    phoneNumber: $phoneNumber
    websiteUrl: $websiteUrl
  ) {
    id
    organizationId
    name
    streetAddress
    city
    state
    postalCode
    countryCode
    phoneNumber
    websiteUrl
    registeredAt
  }
}
    `;
export type RegisterClientMutationFn = Apollo.MutationFunction<RegisterClientMutation, RegisterClientMutationVariables>;

/**
 * __useRegisterClientMutation__
 *
 * To run a mutation, you first call `useRegisterClientMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterClientMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerClientMutation, { data, loading, error }] = useRegisterClientMutation({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *      name: // value for 'name'
 *      streetAddress: // value for 'streetAddress'
 *      city: // value for 'city'
 *      state: // value for 'state'
 *      postalCode: // value for 'postalCode'
 *      countryCode: // value for 'countryCode'
 *      phoneNumber: // value for 'phoneNumber'
 *      websiteUrl: // value for 'websiteUrl'
 *   },
 * });
 */
export function useRegisterClientMutation(baseOptions?: Apollo.MutationHookOptions<RegisterClientMutation, RegisterClientMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterClientMutation, RegisterClientMutationVariables>(RegisterClientDocument, options);
      }
export type RegisterClientMutationHookResult = ReturnType<typeof useRegisterClientMutation>;
export type RegisterClientMutationResult = Apollo.MutationResult<RegisterClientMutation>;
export type RegisterClientMutationOptions = Apollo.BaseMutationOptions<RegisterClientMutation, RegisterClientMutationVariables>;
export const RegisterContactDocument = gql`
    mutation RegisterContact($clientId: UUID!, $name: String!, $email: String, $phoneNumber: String) {
  contact: registerContact(
    clientId: $clientId
    name: $name
    email: $email
    phoneNumber: $phoneNumber
  ) {
    id
    clientId
    name
    email
    phoneNumber
    registeredAt
  }
}
    `;
export type RegisterContactMutationFn = Apollo.MutationFunction<RegisterContactMutation, RegisterContactMutationVariables>;

/**
 * __useRegisterContactMutation__
 *
 * To run a mutation, you first call `useRegisterContactMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterContactMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerContactMutation, { data, loading, error }] = useRegisterContactMutation({
 *   variables: {
 *      clientId: // value for 'clientId'
 *      name: // value for 'name'
 *      email: // value for 'email'
 *      phoneNumber: // value for 'phoneNumber'
 *   },
 * });
 */
export function useRegisterContactMutation(baseOptions?: Apollo.MutationHookOptions<RegisterContactMutation, RegisterContactMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterContactMutation, RegisterContactMutationVariables>(RegisterContactDocument, options);
      }
export type RegisterContactMutationHookResult = ReturnType<typeof useRegisterContactMutation>;
export type RegisterContactMutationResult = Apollo.MutationResult<RegisterContactMutation>;
export type RegisterContactMutationOptions = Apollo.BaseMutationOptions<RegisterContactMutation, RegisterContactMutationVariables>;
export const RegisterOrganizationDocument = gql`
    mutation RegisterOrganization($name: String!, $slug: String!, $logoUrl: String, $streetAddress: String, $city: String, $state: String, $postalCode: String, $countryCode: String, $phoneNumber: String, $websiteUrl: String, $employees: Int, $foundedAt: NaiveDate, $annualRevenueRange: String) {
  organization: registerOrganization(
    name: $name
    slug: $slug
    logoUrl: $logoUrl
    streetAddress: $streetAddress
    city: $city
    state: $state
    postalCode: $postalCode
    countryCode: $countryCode
    phoneNumber: $phoneNumber
    websiteUrl: $websiteUrl
    employees: $employees
    foundedAt: $foundedAt
    annualRevenueRange: $annualRevenueRange
  ) {
    id
    registeredAt
    name
    slug
    logoUrl
    streetAddress
    city
    state
    postalCode
    countryCode
    phoneNumber
    websiteUrl
    employees
    foundedAt
    annualRevenueRange
  }
}
    `;
export type RegisterOrganizationMutationFn = Apollo.MutationFunction<RegisterOrganizationMutation, RegisterOrganizationMutationVariables>;

/**
 * __useRegisterOrganizationMutation__
 *
 * To run a mutation, you first call `useRegisterOrganizationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterOrganizationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerOrganizationMutation, { data, loading, error }] = useRegisterOrganizationMutation({
 *   variables: {
 *      name: // value for 'name'
 *      slug: // value for 'slug'
 *      logoUrl: // value for 'logoUrl'
 *      streetAddress: // value for 'streetAddress'
 *      city: // value for 'city'
 *      state: // value for 'state'
 *      postalCode: // value for 'postalCode'
 *      countryCode: // value for 'countryCode'
 *      phoneNumber: // value for 'phoneNumber'
 *      websiteUrl: // value for 'websiteUrl'
 *      employees: // value for 'employees'
 *      foundedAt: // value for 'foundedAt'
 *      annualRevenueRange: // value for 'annualRevenueRange'
 *   },
 * });
 */
export function useRegisterOrganizationMutation(baseOptions?: Apollo.MutationHookOptions<RegisterOrganizationMutation, RegisterOrganizationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterOrganizationMutation, RegisterOrganizationMutationVariables>(RegisterOrganizationDocument, options);
      }
export type RegisterOrganizationMutationHookResult = ReturnType<typeof useRegisterOrganizationMutation>;
export type RegisterOrganizationMutationResult = Apollo.MutationResult<RegisterOrganizationMutation>;
export type RegisterOrganizationMutationOptions = Apollo.BaseMutationOptions<RegisterOrganizationMutation, RegisterOrganizationMutationVariables>;
export const RegisterUserDocument = gql`
    mutation RegisterUser($organizationId: UUID!, $username: String!, $email: String!, $password: String!, $firstName: String, $lastName: String, $timeZone: String, $language: String, $totpSecret: String) {
  user: registerUser(
    organizationId: $organizationId
    username: $username
    email: $email
    password: $password
    firstName: $firstName
    lastName: $lastName
    timeZone: $timeZone
    language: $language
    totpSecret: $totpSecret
  ) {
    id
    username
    email
    firstName
    lastName
    timeZone
    language
    totpSecret
    registeredAt
  }
}
    `;
export type RegisterUserMutationFn = Apollo.MutationFunction<RegisterUserMutation, RegisterUserMutationVariables>;

/**
 * __useRegisterUserMutation__
 *
 * To run a mutation, you first call `useRegisterUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerUserMutation, { data, loading, error }] = useRegisterUserMutation({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *      username: // value for 'username'
 *      email: // value for 'email'
 *      password: // value for 'password'
 *      firstName: // value for 'firstName'
 *      lastName: // value for 'lastName'
 *      timeZone: // value for 'timeZone'
 *      language: // value for 'language'
 *      totpSecret: // value for 'totpSecret'
 *   },
 * });
 */
export function useRegisterUserMutation(baseOptions?: Apollo.MutationHookOptions<RegisterUserMutation, RegisterUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterUserMutation, RegisterUserMutationVariables>(RegisterUserDocument, options);
      }
export type RegisterUserMutationHookResult = ReturnType<typeof useRegisterUserMutation>;
export type RegisterUserMutationResult = Apollo.MutationResult<RegisterUserMutation>;
export type RegisterUserMutationOptions = Apollo.BaseMutationOptions<RegisterUserMutation, RegisterUserMutationVariables>;
export const SignInDocument = gql`
    mutation SignIn($email: String!, $password: String!) {
  signIn(email: $email, password: $password)
}
    `;
export type SignInMutationFn = Apollo.MutationFunction<SignInMutation, SignInMutationVariables>;

/**
 * __useSignInMutation__
 *
 * To run a mutation, you first call `useSignInMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignInMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signInMutation, { data, loading, error }] = useSignInMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useSignInMutation(baseOptions?: Apollo.MutationHookOptions<SignInMutation, SignInMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SignInMutation, SignInMutationVariables>(SignInDocument, options);
      }
export type SignInMutationHookResult = ReturnType<typeof useSignInMutation>;
export type SignInMutationResult = Apollo.MutationResult<SignInMutation>;
export type SignInMutationOptions = Apollo.BaseMutationOptions<SignInMutation, SignInMutationVariables>;
export const SignOutDocument = gql`
    mutation SignOut {
  signOut
}
    `;
export type SignOutMutationFn = Apollo.MutationFunction<SignOutMutation, SignOutMutationVariables>;

/**
 * __useSignOutMutation__
 *
 * To run a mutation, you first call `useSignOutMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignOutMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signOutMutation, { data, loading, error }] = useSignOutMutation({
 *   variables: {
 *   },
 * });
 */
export function useSignOutMutation(baseOptions?: Apollo.MutationHookOptions<SignOutMutation, SignOutMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SignOutMutation, SignOutMutationVariables>(SignOutDocument, options);
      }
export type SignOutMutationHookResult = ReturnType<typeof useSignOutMutation>;
export type SignOutMutationResult = Apollo.MutationResult<SignOutMutation>;
export type SignOutMutationOptions = Apollo.BaseMutationOptions<SignOutMutation, SignOutMutationVariables>;
export const SignUpDocument = gql`
    mutation SignUp($organizationName: String!, $email: String!, $password: String!, $firstName: String!, $lastName: String!) {
  signUp(
    organizationName: $organizationName
    email: $email
    password: $password
    firstName: $firstName
    lastName: $lastName
  ) {
    organization {
      id
      slug
    }
    user {
      id
      username
    }
    accessToken
  }
}
    `;
export type SignUpMutationFn = Apollo.MutationFunction<SignUpMutation, SignUpMutationVariables>;

/**
 * __useSignUpMutation__
 *
 * To run a mutation, you first call `useSignUpMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignUpMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signUpMutation, { data, loading, error }] = useSignUpMutation({
 *   variables: {
 *      organizationName: // value for 'organizationName'
 *      email: // value for 'email'
 *      password: // value for 'password'
 *      firstName: // value for 'firstName'
 *      lastName: // value for 'lastName'
 *   },
 * });
 */
export function useSignUpMutation(baseOptions?: Apollo.MutationHookOptions<SignUpMutation, SignUpMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SignUpMutation, SignUpMutationVariables>(SignUpDocument, options);
      }
export type SignUpMutationHookResult = ReturnType<typeof useSignUpMutation>;
export type SignUpMutationResult = Apollo.MutationResult<SignUpMutation>;
export type SignUpMutationOptions = Apollo.BaseMutationOptions<SignUpMutation, SignUpMutationVariables>;
export const UpdateClientDocument = gql`
    mutation UpdateClient($id: UUID!, $city: String, $countryCode: String, $name: String, $phoneNumber: String, $postalCode: String, $state: String, $streetAddress: String, $websiteUrl: String) {
  client: updateClient(
    id: $id
    city: $city
    countryCode: $countryCode
    name: $name
    phoneNumber: $phoneNumber
    postalCode: $postalCode
    state: $state
    streetAddress: $streetAddress
    websiteUrl: $websiteUrl
  ) {
    id
    organizationId
    registeredAt
    city
    countryCode
    name
    phoneNumber
    postalCode
    state
    streetAddress
    websiteUrl
  }
}
    `;
export type UpdateClientMutationFn = Apollo.MutationFunction<UpdateClientMutation, UpdateClientMutationVariables>;

/**
 * __useUpdateClientMutation__
 *
 * To run a mutation, you first call `useUpdateClientMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateClientMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateClientMutation, { data, loading, error }] = useUpdateClientMutation({
 *   variables: {
 *      id: // value for 'id'
 *      city: // value for 'city'
 *      countryCode: // value for 'countryCode'
 *      name: // value for 'name'
 *      phoneNumber: // value for 'phoneNumber'
 *      postalCode: // value for 'postalCode'
 *      state: // value for 'state'
 *      streetAddress: // value for 'streetAddress'
 *      websiteUrl: // value for 'websiteUrl'
 *   },
 * });
 */
export function useUpdateClientMutation(baseOptions?: Apollo.MutationHookOptions<UpdateClientMutation, UpdateClientMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateClientMutation, UpdateClientMutationVariables>(UpdateClientDocument, options);
      }
export type UpdateClientMutationHookResult = ReturnType<typeof useUpdateClientMutation>;
export type UpdateClientMutationResult = Apollo.MutationResult<UpdateClientMutation>;
export type UpdateClientMutationOptions = Apollo.BaseMutationOptions<UpdateClientMutation, UpdateClientMutationVariables>;
export const UpdateUserDocument = gql`
    mutation UpdateUser($id: UUID!, $username: String!, $email: String!, $firstName: String, $lastName: String, $timeZone: String, $language: String, $totpSecret: String) {
  user: updateUser(
    id: $id
    username: $username
    email: $email
    firstName: $firstName
    lastName: $lastName
    timeZone: $timeZone
    language: $language
    totpSecret: $totpSecret
  ) {
    id
    username
    email
    firstName
    lastName
    timeZone
    language
    totpSecret
    registeredAt
  }
}
    `;
export type UpdateUserMutationFn = Apollo.MutationFunction<UpdateUserMutation, UpdateUserMutationVariables>;

/**
 * __useUpdateUserMutation__
 *
 * To run a mutation, you first call `useUpdateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserMutation, { data, loading, error }] = useUpdateUserMutation({
 *   variables: {
 *      id: // value for 'id'
 *      username: // value for 'username'
 *      email: // value for 'email'
 *      firstName: // value for 'firstName'
 *      lastName: // value for 'lastName'
 *      timeZone: // value for 'timeZone'
 *      language: // value for 'language'
 *      totpSecret: // value for 'totpSecret'
 *   },
 * });
 */
export function useUpdateUserMutation(baseOptions?: Apollo.MutationHookOptions<UpdateUserMutation, UpdateUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateUserMutation, UpdateUserMutationVariables>(UpdateUserDocument, options);
      }
export type UpdateUserMutationHookResult = ReturnType<typeof useUpdateUserMutation>;
export type UpdateUserMutationResult = Apollo.MutationResult<UpdateUserMutation>;
export type UpdateUserMutationOptions = Apollo.BaseMutationOptions<UpdateUserMutation, UpdateUserMutationVariables>;
export const FindClientsDocument = gql`
    query FindClients($organizationId: UUID!) {
  clients: findClients(organizationId: $organizationId) {
    id
    organizationId
    registeredAt
    name
    streetAddress
    city
    state
    postalCode
    countryCode
    phoneNumber
    websiteUrl
  }
}
    `;

/**
 * __useFindClientsQuery__
 *
 * To run a query within a React component, call `useFindClientsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFindClientsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFindClientsQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *   },
 * });
 */
export function useFindClientsQuery(baseOptions: Apollo.QueryHookOptions<FindClientsQuery, FindClientsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FindClientsQuery, FindClientsQueryVariables>(FindClientsDocument, options);
      }
export function useFindClientsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FindClientsQuery, FindClientsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FindClientsQuery, FindClientsQueryVariables>(FindClientsDocument, options);
        }
export type FindClientsQueryHookResult = ReturnType<typeof useFindClientsQuery>;
export type FindClientsLazyQueryHookResult = ReturnType<typeof useFindClientsLazyQuery>;
export type FindClientsQueryResult = Apollo.QueryResult<FindClientsQuery, FindClientsQueryVariables>;
export const FindContactsDocument = gql`
    query FindContacts($clientId: UUID!) {
  contacts: findContacts(clientId: $clientId) {
    id
    clientId
    name
    email
    phoneNumber
    registeredAt
  }
}
    `;

/**
 * __useFindContactsQuery__
 *
 * To run a query within a React component, call `useFindContactsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFindContactsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFindContactsQuery({
 *   variables: {
 *      clientId: // value for 'clientId'
 *   },
 * });
 */
export function useFindContactsQuery(baseOptions: Apollo.QueryHookOptions<FindContactsQuery, FindContactsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FindContactsQuery, FindContactsQueryVariables>(FindContactsDocument, options);
      }
export function useFindContactsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FindContactsQuery, FindContactsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FindContactsQuery, FindContactsQueryVariables>(FindContactsDocument, options);
        }
export type FindContactsQueryHookResult = ReturnType<typeof useFindContactsQuery>;
export type FindContactsLazyQueryHookResult = ReturnType<typeof useFindContactsLazyQuery>;
export type FindContactsQueryResult = Apollo.QueryResult<FindContactsQuery, FindContactsQueryVariables>;
export const FindOrganizationsDocument = gql`
    query FindOrganizations {
  organizations: findOrganizations {
    id
    registeredAt
    name
    slug
    annualRevenueRange
    city
    countryCode
    employees
    foundedAt
    logoUrl
    phoneNumber
    postalCode
    state
    streetAddress
    websiteUrl
  }
}
    `;

/**
 * __useFindOrganizationsQuery__
 *
 * To run a query within a React component, call `useFindOrganizationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFindOrganizationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFindOrganizationsQuery({
 *   variables: {
 *   },
 * });
 */
export function useFindOrganizationsQuery(baseOptions?: Apollo.QueryHookOptions<FindOrganizationsQuery, FindOrganizationsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FindOrganizationsQuery, FindOrganizationsQueryVariables>(FindOrganizationsDocument, options);
      }
export function useFindOrganizationsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FindOrganizationsQuery, FindOrganizationsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FindOrganizationsQuery, FindOrganizationsQueryVariables>(FindOrganizationsDocument, options);
        }
export type FindOrganizationsQueryHookResult = ReturnType<typeof useFindOrganizationsQuery>;
export type FindOrganizationsLazyQueryHookResult = ReturnType<typeof useFindOrganizationsLazyQuery>;
export type FindOrganizationsQueryResult = Apollo.QueryResult<FindOrganizationsQuery, FindOrganizationsQueryVariables>;
export const FindUsersDocument = gql`
    query FindUsers($organizationId: UUID!) {
  users: findUsers(organizationId: $organizationId) {
    id
    registeredAt
    username
    email
    firstName
    lastName
    timeZone
    language
    totpSecret
  }
}
    `;

/**
 * __useFindUsersQuery__
 *
 * To run a query within a React component, call `useFindUsersQuery` and pass it any options that fit your needs.
 * When your component renders, `useFindUsersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFindUsersQuery({
 *   variables: {
 *      organizationId: // value for 'organizationId'
 *   },
 * });
 */
export function useFindUsersQuery(baseOptions: Apollo.QueryHookOptions<FindUsersQuery, FindUsersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FindUsersQuery, FindUsersQueryVariables>(FindUsersDocument, options);
      }
export function useFindUsersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FindUsersQuery, FindUsersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FindUsersQuery, FindUsersQueryVariables>(FindUsersDocument, options);
        }
export type FindUsersQueryHookResult = ReturnType<typeof useFindUsersQuery>;
export type FindUsersLazyQueryHookResult = ReturnType<typeof useFindUsersLazyQuery>;
export type FindUsersQueryResult = Apollo.QueryResult<FindUsersQuery, FindUsersQueryVariables>;
export const GetClientDocument = gql`
    query GetClient($id: UUID!) {
  client: getClient(id: $id) {
    id
    organizationId
    registeredAt
    name
    streetAddress
    city
    state
    postalCode
    countryCode
    phoneNumber
    websiteUrl
    contacts {
      id
      name
      email
    }
  }
}
    `;

/**
 * __useGetClientQuery__
 *
 * To run a query within a React component, call `useGetClientQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetClientQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetClientQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetClientQuery(baseOptions: Apollo.QueryHookOptions<GetClientQuery, GetClientQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetClientQuery, GetClientQueryVariables>(GetClientDocument, options);
      }
export function useGetClientLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetClientQuery, GetClientQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetClientQuery, GetClientQueryVariables>(GetClientDocument, options);
        }
export type GetClientQueryHookResult = ReturnType<typeof useGetClientQuery>;
export type GetClientLazyQueryHookResult = ReturnType<typeof useGetClientLazyQuery>;
export type GetClientQueryResult = Apollo.QueryResult<GetClientQuery, GetClientQueryVariables>;
export const GetOrganizationDocument = gql`
    query GetOrganization($id: UUID!) {
  organization: getOrganization(id: $id) {
    id
    registeredAt
    slug
    name
    logoUrl
    streetAddress
    city
    state
    postalCode
    countryCode
    phoneNumber
    websiteUrl
    employees
    foundedAt
    annualRevenueRange
  }
}
    `;

/**
 * __useGetOrganizationQuery__
 *
 * To run a query within a React component, call `useGetOrganizationQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetOrganizationQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetOrganizationQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetOrganizationQuery(baseOptions: Apollo.QueryHookOptions<GetOrganizationQuery, GetOrganizationQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetOrganizationQuery, GetOrganizationQueryVariables>(GetOrganizationDocument, options);
      }
export function useGetOrganizationLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetOrganizationQuery, GetOrganizationQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetOrganizationQuery, GetOrganizationQueryVariables>(GetOrganizationDocument, options);
        }
export type GetOrganizationQueryHookResult = ReturnType<typeof useGetOrganizationQuery>;
export type GetOrganizationLazyQueryHookResult = ReturnType<typeof useGetOrganizationLazyQuery>;
export type GetOrganizationQueryResult = Apollo.QueryResult<GetOrganizationQuery, GetOrganizationQueryVariables>;
export const GetPublicOrganizationDocument = gql`
    query GetPublicOrganization($slug: String!) {
  organization: getPublicOrganization(slug: $slug) {
    id
    slug
    name
  }
}
    `;

/**
 * __useGetPublicOrganizationQuery__
 *
 * To run a query within a React component, call `useGetPublicOrganizationQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPublicOrganizationQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPublicOrganizationQuery({
 *   variables: {
 *      slug: // value for 'slug'
 *   },
 * });
 */
export function useGetPublicOrganizationQuery(baseOptions: Apollo.QueryHookOptions<GetPublicOrganizationQuery, GetPublicOrganizationQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPublicOrganizationQuery, GetPublicOrganizationQueryVariables>(GetPublicOrganizationDocument, options);
      }
export function useGetPublicOrganizationLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPublicOrganizationQuery, GetPublicOrganizationQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPublicOrganizationQuery, GetPublicOrganizationQueryVariables>(GetPublicOrganizationDocument, options);
        }
export type GetPublicOrganizationQueryHookResult = ReturnType<typeof useGetPublicOrganizationQuery>;
export type GetPublicOrganizationLazyQueryHookResult = ReturnType<typeof useGetPublicOrganizationLazyQuery>;
export type GetPublicOrganizationQueryResult = Apollo.QueryResult<GetPublicOrganizationQuery, GetPublicOrganizationQueryVariables>;