import React from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import EmptyList from '../EmptyList';
import { useFindUsersQuery } from '../generated/graphql';
import Spinner from '../Spinner';

interface Props {
  organizationId: string;
}

export default function Users({ organizationId }: Props): JSX.Element {
  const { url } = useRouteMatch();
  const { data, loading, error } = useFindUsersQuery({ variables: { organizationId } });

  if (loading) return <Spinner />;
  if (error) return <div>error! {error?.message}</div>;
  if (!data?.users.length) {
    return (
      <EmptyList
        title="No users"
        description="Get started by creating a new user."
        callToAction={{ text: 'New User', to: `${url}/new` }}
      />
    );
  }

  return (
    <div className="flex flex-col">
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
          <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="bg-gray-50">
                <tr>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Name
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Email
                  </th>
                  <th scope="col" className="relative px-6 py-3">
                    <span className="sr-only">Edit</span>
                  </th>
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {data.users.map((user) => (
                  <tr key={user.id}>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="flex items-center">
                        <div className="ml-4 text-sm font-medium text-gray-900">
                          <Link
                            to={`${url}/${user.id}`}
                            className="text-indigo-600 hover:text-indigo-900"
                          >
                            {user.firstName} {user.lastName}
                          </Link>
                        </div>
                      </div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                      {user.email}
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                      <Link
                        to={`${url}/${user.id}/edit`}
                        className="text-indigo-600 hover:text-indigo-900"
                      >
                        Edit
                      </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}
