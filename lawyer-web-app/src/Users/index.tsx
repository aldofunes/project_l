import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import Main from '../Layout/Main';
import Header from './Header';
import UserList from './UserList';
import NewUser from './NewUser';

interface Props {
  organizationId: string;
}

export default function Users({ organizationId }: Props): JSX.Element {
  const { path } = useRouteMatch();

  return (
    <>
      <Header />
      <Main>
        <Switch>
          <Route path={`${path}/new`}>
            <NewUser organizationId={organizationId} />
          </Route>
          <Route path={`${path}`}>
            <UserList organizationId={organizationId} />
          </Route>
        </Switch>
      </Main>
    </>
  );
}
