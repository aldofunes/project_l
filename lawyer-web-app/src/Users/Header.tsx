import { ChevronLeftIcon } from '@heroicons/react/solid';
import React from 'react';
import { Link, Route, Switch, useRouteMatch } from 'react-router-dom';
import { Breadcrumb as NewUserBreadcrumb } from './NewUser';

export default function Header(): JSX.Element {
  const { url, path } = useRouteMatch();

  return (
    <div className="p-9 ">
      <div>
        <nav className="sm:hidden" aria-label="Back">
          <Link
            to={`${url}`}
            className="flex items-center text-sm font-medium text-gray-600 hover:text-gray-900 dark:text-gray-400 hover:dark:text-gray-200"
          >
            <ChevronLeftIcon
              className="flex-shrink-0 -ml-1 mr-1 h-5 w-5 text-gray-500"
              aria-hidden="true"
            />
            Back
          </Link>
        </nav>
        <Switch>
          <Route path={`${path}/new`}>
            <NewUserBreadcrumb />
          </Route>
        </Switch>
      </div>
      <div className="mt-2 md:flex md:items-center md:justify-between">
        <div className="flex-1 min-w-0">
          <h2 className="text-2xl font-bold leading-7 text-black dark:text-white sm:text-3xl sm:truncate">
            Users
          </h2>
        </div>
        <div className="mt-4 flex-shrink-0 flex md:mt-0 md:ml-4">
          <Link
            to={`${url}/new`}
            type="button"
            className="ml-3 inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-500 hover:bg-indigo-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-indigo-500"
          >
            New
          </Link>
        </div>
      </div>
    </div>
  );
}
