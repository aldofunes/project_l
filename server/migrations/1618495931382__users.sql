CREATE TABLE users (
  "id" uuid PRIMARY KEY,
  "registered_at" timestamp NOT NULL,
  "username" text NOT NULL UNIQUE,
  "email" text NOT NULL UNIQUE,
  "password_hash" text NOT NULL,
  "first_name" text,
  "last_name" text,
  "time_zone" text,
  "language" text,
  "totp_secret" text
);

