CREATE TABLE documents (
  "id" uuid PRIMARY KEY,
  "client_id" uuid NOT NULL REFERENCES clients,
  "name" text NOT NULL,
  "url" text NOT NULL,
  "uploaded_at" timestamp NOT NULL
);

