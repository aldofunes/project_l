CREATE TABLE organizations (
  "id" uuid PRIMARY KEY,
  "registered_at" timestamp NOT NULL,
  "slug" text UNIQUE NOT NULL,
  "name" text NOT NULL,
  "logo_url" text,
  "street_address" text,
  "city" text,
  "state" text,
  "postal_code" text,
  "country_code" text,
  "phone_number" text,
  "website_url" text,
  "employees" integer,
  "founded_at" date,
  "annual_revenue_range" text
);

