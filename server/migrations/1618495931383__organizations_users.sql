CREATE TABLE organizations_users (
  "organization_id" uuid NOT NULL REFERENCES organizations,
  "user_id" uuid NOT NULL REFERENCES users,
  PRIMARY KEY ("organization_id", "user_id")
);

