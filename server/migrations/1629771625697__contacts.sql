CREATE TABLE contacts (
  "id" uuid PRIMARY KEY,
  "client_id" uuid NOT NULL REFERENCES clients,
  "registered_at" timestamp NOT NULL,
  "name" text NOT NULL,
  "email" text,
  "phone_number" text
);

