CREATE TABLE clients (
  "id" uuid PRIMARY KEY,
  "organization_id" uuid NOT NULL REFERENCES organizations,
  "registered_at" timestamp NOT NULL,
  "name" text,
  "street_address" text,
  "city" text,
  "state" text,
  "postal_code" text,
  "country_code" text,
  "phone_number" text,
  "website_url" text
);

