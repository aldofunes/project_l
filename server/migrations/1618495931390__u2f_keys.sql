CREATE TABLE u2f_keys (
  "user_id" uuid REFERENCES users,
  "public_key" text,
  "key_handle" text,
  "version" text DEFAULT 'U2F_V2',
  "app_id" text,
  PRIMARY KEY ("user_id", "key_handle")
);

