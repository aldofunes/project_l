use crate::{error::Error, objects::Client, ports::ClientPort};
use async_trait::async_trait;
use sqlx::PgPool;
use uuid::Uuid;

pub struct ClientGatewayPostgres<'a> {
  pool: &'a PgPool,
}

impl<'a> ClientGatewayPostgres<'a> {
  pub fn new(pool: &'a PgPool) -> Self {
    Self { pool }
  }
}

#[async_trait]
impl<'a> ClientPort for ClientGatewayPostgres<'a> {
  async fn find(&self, organization_id: Uuid) -> Result<Vec<Client>, Error> {
    let clients = sqlx::query_as("select * from clients where organization_id = $1")
      .bind(organization_id)
      .fetch_all(self.pool)
      .await?;

    return Ok(clients);
  }

  async fn get(&self, id: Uuid) -> Result<Option<Client>, Error> {
    let client = sqlx::query_as("select * from clients where id = $1")
      .bind(id)
      .fetch_optional(self.pool)
      .await?;

    return Ok(client);
  }

  async fn insert(&self, client: Client) -> Result<Client, Error> {
    sqlx::query(
      "
        INSERT INTO clients (
          id,
          organization_id,
          registered_at,
          name,
          street_address,
          city,
          state,
          postal_code,
          country_code,
          phone_number,
          website_url
        )
        VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)
      ",
    )
    .bind(client.id)
    .bind(client.organization_id)
    .bind(client.registered_at)
    .bind(client.name)
    .bind(client.street_address)
    .bind(client.city)
    .bind(client.state)
    .bind(client.postal_code)
    .bind(client.country_code)
    .bind(client.phone_number)
    .bind(client.website_url)
    .fetch_optional(self.pool)
    .await?;

    let new_client = sqlx::query_as("select * from clients where id = $1")
      .bind(client.id)
      .fetch_one(self.pool)
      .await?;

    Ok(new_client)
  }

  async fn update(&self, client: Client) -> Result<Client, Error> {
    sqlx::query(
      "
        UPDATE
          clients
        SET
          name = $2,
          street_address = $3,
          city = $4,
          state = $5,
          postal_code = $6,
          country_code = $7,
          phone_number = $8,
          website_url = $9
        WHERE
          id = $1
      ",
    )
    .bind(client.id)
    .bind(client.name)
    .bind(client.street_address)
    .bind(client.city)
    .bind(client.state)
    .bind(client.postal_code)
    .bind(client.country_code)
    .bind(client.phone_number)
    .bind(client.website_url)
    .fetch_optional(self.pool)
    .await?;

    let new_client = sqlx::query_as("select * from clients where id = $1")
      .bind(client.id)
      .fetch_one(self.pool)
      .await?;

    Ok(new_client)
  }

  async fn delete(&self, client: Client) -> Result<(), Error> {
    sqlx::query("DELETE FROM clients WHERE id = $1")
      .bind(client.id)
      .fetch_optional(self.pool)
      .await?;

    Ok(())
  }
}
