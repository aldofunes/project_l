use crate::{
  error::Error,
  objects::{Organization, User},
  ports::OrganizationPort,
};
use async_trait::async_trait;
use sqlx::PgPool;
use uuid::Uuid;

pub struct OrganizationGatewayPostgres<'a> {
  pool: &'a PgPool,
}

impl<'a> OrganizationGatewayPostgres<'a> {
  pub fn new(pool: &'a PgPool) -> Self {
    Self { pool }
  }
}

#[async_trait]
impl<'a> OrganizationPort for OrganizationGatewayPostgres<'a> {
  async fn find(&self) -> Result<Vec<Organization>, Error> {
    let organizations = sqlx::query_as("select * from organizations")
      .fetch_all(self.pool)
      .await?;

    Ok(organizations)
  }

  async fn find_organizations_for_user(&self, user_id: Uuid) -> Result<Vec<Organization>, Error> {
    let organizations = sqlx::query_as(
      "
        SELECT
          organizations.*
        FROM
          organizations
          INNER JOIN organizations_users ON organizations_users.organization_id = organizations.id
        WHERE
          organizations_users.user_id = $1
    ",
    )
    .bind(user_id)
    .fetch_all(self.pool)
    .await?;

    Ok(organizations)
  }

  async fn get(&self, id: Uuid) -> Result<Option<Organization>, Error> {
    let organization = sqlx::query_as("select * from organizations where id = $1")
      .bind(id)
      .fetch_optional(self.pool)
      .await?;

    Ok(organization)
  }

  async fn get_by_slug(&self, slug: String) -> Result<Option<Organization>, Error> {
    let organization = sqlx::query_as("select * from organizations where slug = $1")
      .bind(slug)
      .fetch_optional(self.pool)
      .await?;

    Ok(organization)
  }

  async fn insert(&self, organization: Organization) -> Result<Organization, Error> {
    sqlx::query(
      "
        INSERT INTO organizations (
          id,
          registered_at,
          slug,
          name,
          logo_url,
          street_address,
          city,
          state,
          postal_code,
          country_code,
          phone_number,
          website_url,
          employees,
          founded_at,
          annual_revenue_range
        )
        VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15)
      ",
    )
    .bind(organization.id)
    .bind(organization.registered_at)
    .bind(organization.slug)
    .bind(organization.name)
    .bind(organization.logo_url)
    .bind(organization.street_address)
    .bind(organization.city)
    .bind(organization.state)
    .bind(organization.postal_code)
    .bind(organization.country_code)
    .bind(organization.phone_number)
    .bind(organization.website_url)
    .bind(organization.employees)
    .bind(organization.founded_at)
    .bind(organization.annual_revenue_range)
    .fetch_optional(self.pool)
    .await?;

    let new_organization = sqlx::query_as("select * from organizations where id = $1")
      .bind(organization.id)
      .fetch_one(self.pool)
      .await?;

    Ok(new_organization)
  }

  async fn update(&self, organization: Organization) -> Result<Organization, Error> {
    sqlx::query(
      "
        UPDATE
          organizations
        SET
          slug = $2,
          name = $3,
          logo_url = $4,
          street_address = $5,
          city = $6,
          state = $7,
          postal_code = $8,
          country_code = $9,
          phone_number = $10,
          website_url = $11,
          employees = $12,
          founded_at = $13,
          annual_revenue_range = $14
        WHERE
          id = $1
      ",
    )
    .bind(organization.id)
    .bind(organization.slug)
    .bind(organization.name)
    .bind(organization.logo_url)
    .bind(organization.street_address)
    .bind(organization.city)
    .bind(organization.state)
    .bind(organization.postal_code)
    .bind(organization.country_code)
    .bind(organization.phone_number)
    .bind(organization.website_url)
    .bind(organization.employees)
    .bind(organization.founded_at)
    .bind(organization.annual_revenue_range)
    .fetch_optional(self.pool)
    .await?;

    let updated_organization = sqlx::query_as("select * from organizations where id = $1")
      .bind(organization.id)
      .fetch_one(self.pool)
      .await?;

    Ok(updated_organization)
  }

  async fn delete(&self, organization: Organization) -> Result<(), Error> {
    sqlx::query("DELETE FROM organizations WHERE id = $1")
      .bind(organization.id)
      .fetch_optional(self.pool)
      .await?;

    Ok(())
  }

  async fn add_user_to_organization(&self, organization: &Organization, user: &User) -> Result<(), Error> {
    sqlx::query("INSERT INTO organizations_users (organization_id, user_id) VALUES ($1,$2)")
      .bind(organization.id)
      .bind(user.id)
      .fetch_optional(self.pool)
      .await?;

    Ok(())
  }

  async fn remove_user_from_organization(&self, organization: &Organization, user: &User) -> Result<(), Error> {
    sqlx::query("DELETE FROM organizations_users WHERE organization_id = $1 and user_id = $2")
      .bind(organization.id)
      .bind(user.id)
      .fetch_optional(self.pool)
      .await?;

    Ok(())
  }
}
