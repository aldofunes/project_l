use crate::{error::Error, objects::Contact, ports::ContactPort};
use async_trait::async_trait;
use sqlx::PgPool;
use uuid::Uuid;

pub struct ContactGatewayPostgres<'a> {
  pool: &'a PgPool,
}

impl<'a> ContactGatewayPostgres<'a> {
  pub fn new(pool: &'a PgPool) -> Self {
    Self { pool }
  }
}

#[async_trait]
impl<'a> ContactPort for ContactGatewayPostgres<'a> {
  async fn find(&self, client_id: Uuid) -> Result<Vec<Contact>, Error> {
    let contacts = sqlx::query_as("select * from contacts where client_id = $1")
      .bind(client_id)
      .fetch_all(self.pool)
      .await?;

    return Ok(contacts);
  }

  async fn get(&self, id: Uuid) -> Result<Option<Contact>, Error> {
    let contact = sqlx::query_as("select * from contacts where id = $1")
      .bind(id)
      .fetch_optional(self.pool)
      .await?;

    return Ok(contact);
  }

  async fn insert(&self, contact: Contact) -> Result<Contact, Error> {
    sqlx::query(
      "
        INSERT INTO contacts (
          id,
          client_id,
          name,
          email,
          phone_number
        )
        VALUES ($1,$2,$3,$4,$5)
      ",
    )
    .bind(contact.id)
    .bind(contact.client_id)
    .bind(contact.name)
    .bind(contact.email)
    .bind(contact.phone_number)
    .fetch_optional(self.pool)
    .await?;

    let new_contact = sqlx::query_as("select * from contacts where id = $1")
      .bind(contact.id)
      .fetch_one(self.pool)
      .await?;

    Ok(new_contact)
  }

  async fn update(&self, contact: Contact) -> Result<Contact, Error> {
    sqlx::query(
      "
        UPDATE
          contacts
        SET
          name = $2,
          email = $3,
          phone_number = $4,
        WHERE
          id = $1
      ",
    )
    .bind(contact.id)
    .bind(contact.name)
    .bind(contact.email)
    .bind(contact.phone_number)
    .fetch_optional(self.pool)
    .await?;

    let updated_contact = sqlx::query_as("select * from contacts where id = $1")
      .bind(contact.id)
      .fetch_one(self.pool)
      .await?;

    Ok(updated_contact)
  }

  async fn delete(&self, contact: Contact) -> Result<(), Error> {
    sqlx::query("DELETE FROM contacts WHERE id = $1")
      .bind(contact.id)
      .fetch_optional(self.pool)
      .await?;

    Ok(())
  }
}
