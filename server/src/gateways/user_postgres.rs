use crate::{
  error::Error,
  objects::{Organization, User},
  ports::UserPort,
};
use async_trait::async_trait;
use sqlx::PgPool;
use uuid::Uuid;

pub struct UserGatewayPostgres<'a> {
  pool: &'a PgPool,
}

impl<'a> UserGatewayPostgres<'a> {
  pub fn new(pool: &'a PgPool) -> Self {
    Self { pool }
  }
}

#[async_trait]
impl<'a> UserPort for UserGatewayPostgres<'a> {
  async fn find(&self, organization_id: Uuid) -> Result<Vec<User>, Error> {
    let users = sqlx::query_as(
      "
      SELECT
        users.*
      FROM
        users
        INNER JOIN organizations_users ON organizations_users.user_id = users.id
      WHERE
        organizations_users.organization_id = $1
    ",
    )
    .bind(organization_id)
    .fetch_all(self.pool)
    .await?;

    Ok(users)
  }

  async fn get(&self, id: Uuid) -> Result<Option<User>, Error> {
    let user = sqlx::query_as("select * from users where id = $1")
      .bind(id)
      .fetch_optional(self.pool)
      .await?;

    Ok(user)
  }

  async fn get_by_email(&self, email: String) -> Result<Option<User>, Error> {
    let user = sqlx::query_as("select * from users where email = $1")
      .bind(email)
      .fetch_optional(self.pool)
      .await?;

    Ok(user)
  }

  async fn get_by_username(&self, username: String) -> Result<Option<User>, Error> {
    let user = sqlx::query_as("select * from users where username = $1")
      .bind(username)
      .fetch_optional(self.pool)
      .await?;

    Ok(user)
  }

  async fn insert(&self, user: User) -> Result<User, Error> {
    sqlx::query(
      "
        INSERT INTO users (
          id,
          registered_at,
          username,
          email,
          password_hash,
          first_name,
          last_name,
          time_zone,
          language,
          totp_secret
        )
        VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)
      ",
    )
    .bind(user.id)
    .bind(user.registered_at)
    .bind(user.username)
    .bind(user.email)
    .bind(user.password_hash)
    .bind(user.first_name)
    .bind(user.last_name)
    .bind(user.time_zone)
    .bind(user.language)
    .bind(user.totp_secret)
    .fetch_optional(self.pool)
    .await?;

    let new_user = sqlx::query_as("select * from users where id = $1")
      .bind(user.id)
      .fetch_one(self.pool)
      .await?;

    Ok(new_user)
  }

  async fn update(&self, user: User) -> Result<User, Error> {
    sqlx::query(
      "
        UPDATE
          users
        SET
          username = $2,
          email = $3,
          password_hash = $4,
          first_name = $5,
          last_name = $6,
          time_zone = $7,
          language = $8,
          totp_secret = $9
        WHERE
          id = $1
      ",
    )
    .bind(user.id)
    .bind(user.username)
    .bind(user.email)
    .bind(user.password_hash)
    .bind(user.first_name)
    .bind(user.last_name)
    .bind(user.time_zone)
    .bind(user.language)
    .bind(user.totp_secret)
    .fetch_optional(self.pool)
    .await?;

    let updated_user = sqlx::query_as("select * from users where id = $1")
      .bind(user.id)
      .fetch_one(self.pool)
      .await?;

    Ok(updated_user)
  }

  async fn delete(&self, user: User) -> Result<(), Error> {
    sqlx::query("DELETE FROM users WHERE id = $1")
      .bind(user.id)
      .fetch_optional(self.pool)
      .await?;

    Ok(())
  }

  async fn add_organization_to_user(&self, user: &User, organization: &Organization) -> Result<(), Error> {
    sqlx::query("INSERT INTO organizations_users (organization_id, user_id) VALUES ($1,$2)")
      .bind(organization.id)
      .bind(user.id)
      .fetch_optional(self.pool)
      .await?;

    Ok(())
  }
  async fn remove_organization_from_user(&self, user: &User, organization: &Organization) -> Result<(), Error> {
    sqlx::query("DELETE FROM organizations_users WHERE organization_id = $1 and user_id = $2")
      .bind(organization.id)
      .bind(user.id)
      .fetch_optional(self.pool)
      .await?;

    Ok(())
  }
}
