mod client_postgres;
mod contact_postgres;
mod organization_postgres;
mod user_postgres;

pub use client_postgres::ClientGatewayPostgres;
pub use contact_postgres::ContactGatewayPostgres;
pub use organization_postgres::OrganizationGatewayPostgres;
pub use user_postgres::UserGatewayPostgres;
