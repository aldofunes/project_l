use crate::{
  config::Config,
  error::Error,
  graphql_ctx::{GlobalContext, RequestContext},
  mutations::{
    authentication::AuthenticationMutation, client::ClientMutation, contact::ContactMutation,
    organization::OrganizationMutation, user::UserMutation,
  },
  objects::JwtClaims,
  queries::{
    client::ClientQuery, contact::ContactQuery, organization::OrganizationQuery, user::UserQuery,
    public_organization::PublicOrganizationQuery,
  },
};
use ::http::header::{AUTHORIZATION, CONTENT_TYPE};
use argon2::Argon2;
use async_graphql::{
  extensions::ApolloTracing,
  http::{playground_source, GraphQLPlaygroundConfig},
  Data, EmptySubscription, MergedObject, Schema,
};
use async_graphql_warp::{graphql_subscription_with_data, Response};
use jsonwebtoken::dangerous_insecure_decode;
use regex::Regex;
use serde::{Deserialize, Serialize};
use sqlx::PgPool;
use std::convert::Infallible;
use warp::{
  http::{Method, Response as HttpResponse},
  Filter,
};

#[derive(MergedObject, Default)]
struct RootQuery(
  ClientQuery,
  ContactQuery,
  OrganizationQuery,
  UserQuery,
  PublicOrganizationQuery,
);

#[derive(MergedObject, Default)]
struct RootMutation(
  AuthenticationMutation,
  ClientMutation,
  ContactMutation,
  OrganizationMutation,
  UserMutation,
);

/// An API error serializable to JSON.
#[derive(Serialize)]
struct ErrorMessage {
  code: u16,
  message: String,
}

pub async fn start() -> Result<(), Error> {
  let config = Config::new()?;

  log::trace!("opening db pool");
  let db_pool = PgPool::connect(&config.connection_uri).await?;

  let argon2 = Argon2::default();

  let context = GlobalContext {
    config,
    db_pool,
    argon2,
  };

  log::trace!("create schema");
  let schema = Schema::build(
    RootQuery::default(),
    RootMutation::default(),
    EmptySubscription,
  )
  .limit_complexity(100)
  .limit_depth(10)
  .data(context)
  .extension(ApolloTracing)
  .finish();

  let graphql_post = warp::path("graphql")
    .and(warp::post())
    .and(warp::header::optional::<String>("Authorization"))
    .and(warp::cookie::optional::<String>("refresh_token"))
    .and(async_graphql_warp::graphql(schema.clone()))
    .and_then(
      |auth_token: Option<String>,
       refresh_token: Option<String>,
       (schema, mut request): (
        Schema<RootQuery, RootMutation, EmptySubscription>,
        async_graphql::Request,
      )| async move {
        let mut request_context = RequestContext {
          access_token: None,
          refresh_token,
          user_id: None,
          organization_ids: None,
        };

        if let Some(auth_token) = auth_token {
          let bearer_token_regex =
            Regex::new(r"^Bearer [A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$").unwrap();

          if bearer_token_regex.is_match(&auth_token) {
            let access_token = auth_token.replace("Bearer ", "");
            if let Ok(decoded_token) = dangerous_insecure_decode::<JwtClaims>(&access_token) {
              request_context.access_token = Some(access_token);
              request_context.user_id = Some(decoded_token.claims.sub);
              request_context.organization_ids = Some(decoded_token.claims.organization_ids);
            }
          }
        }

        request = request.data(request_context);

        let response = Response::from(schema.execute(request).await);
        Ok::<_, Infallible>(response)
      },
    );

  let graphql_playground = warp::path("graphql").and(warp::get()).map(|| {
    HttpResponse::builder()
      .header("content-type", "text/html")
      .body(playground_source(
        GraphQLPlaygroundConfig::new("/graphql").subscription_endpoint("/graphql"),
      ))
  });

  let graphql_subscription = graphql_subscription_with_data(schema, |value| async {
    #[derive(Deserialize)]
    struct Payload {
      authorization: String,
    }

    if let Ok(payload) = serde_json::from_value::<Payload>(value) {
      let mut data = Data::default();
      let request_context = RequestContext {
        access_token: Some(payload.authorization),
        refresh_token: None,
        user_id: None,
        organization_ids: None,
      };
      data.insert(request_context);

      Ok(data)
    } else {
      Err("Token is required".into())
    }
  });

  let cors = warp::cors()
    .allow_credentials(true)
    .allow_origin("http://localhost:8000")
    .allow_origin("http://localhost:3000")
    .allow_origin("https://aldofunes.ngrok.io")
    .allow_origin("https://project-l.ngrok.io")
    .allow_method(Method::GET)
    .allow_method(Method::POST)
    .allow_header(AUTHORIZATION)
    .allow_header(CONTENT_TYPE);

  let routes = graphql_playground
    .or(graphql_subscription)
    .or(graphql_post)
    .or(warp::any().map(warp::reply))
    .with(cors);

  log::info!("Playground: http://localhost:8000/graphql");
  warp::serve(routes).run(([0, 0, 0, 0], 8000)).await;

  Ok(())
}
