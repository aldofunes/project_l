use crate::{
  error::Error,
  gateways::{OrganizationGatewayPostgres, UserGatewayPostgres},
  graphql_ctx,
  guards::AuthenticationGuard,
  objects::User,
  ports::{OrganizationPort, UserPort},
};
use argon2::password_hash::{PasswordHasher, SaltString};
use async_graphql::{guard::Guard, Context, ErrorExtensions, Object, Result};
use chrono::Utc;
use rand_core::OsRng;
use uuid::Uuid;

#[derive(Default)]
pub struct UserQuery;

#[derive(Default)]
pub struct UserMutation;

#[Object]
impl UserMutation {
  #[graphql(guard(AuthenticationGuard()))]
  #[allow(clippy::too_many_arguments)]
  async fn register_user(
    &self,
    ctx: &Context<'_>,
    organization_id: Uuid,
    username: String,
    email: String,
    password: String,
    first_name: Option<String>,
    last_name: Option<String>,
    time_zone: Option<String>,
    language: Option<String>,
    totp_secret: Option<String>,
  ) -> Result<User> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let user_gateway = UserGatewayPostgres::new(&global_context.db_pool);
    let organization_gateway = OrganizationGatewayPostgres::new(&global_context.db_pool);

    let salt = SaltString::generate(&mut OsRng);

    log::trace!("hash password");
    let password_hash = global_context
      .argon2
      .hash_password(password.as_bytes(), salt.as_ref())?
      .to_string();

    let user = user_gateway
      .insert(User {
        id: Uuid::new_v4(),
        registered_at: Utc::now().naive_utc(),
        email,
        username,
        password_hash,
        first_name,
        last_name,
        time_zone,
        language,
        totp_secret,
      })
      .await
      .map_err(|e| e.extend())?;

    let organization = organization_gateway
      .get(organization_id)
      .await
      .map_err(|e| e.extend())?
      .ok_or_else(|| Error::NotFound(format!("organization {}", organization_id)))?;

    user_gateway
      .add_organization_to_user(&user, &organization)
      .await
      .map_err(|e| e.extend())?;

    Ok(user)
  }

  #[graphql(guard(AuthenticationGuard()))]
  #[allow(clippy::too_many_arguments)]
  async fn update_user(
    &self,
    ctx: &Context<'_>,
    id: Uuid,
    username: Option<String>,
    email: Option<String>,
    first_name: Option<String>,
    last_name: Option<String>,
    time_zone: Option<String>,
    language: Option<String>,
    totp_secret: Option<String>,
  ) -> Result<User> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let user_gateway = UserGatewayPostgres::new(&global_context.db_pool);

    let mut user = user_gateway
      .get(id)
      .await
      .map_err(|e| e.extend())?
      .ok_or_else(|| Error::NotFound(format!("user {}", id)).extend())?;

    if let Some(username) = username {
      user.username = username;
    };
    if let Some(email) = email {
      user.email = email;
    };

    user.first_name = first_name;
    user.last_name = last_name;
    user.time_zone = time_zone;
    user.language = language;
    user.totp_secret = totp_secret;

    let user = user_gateway
      .update(user)
      .await
      .map_err(|e| e.extend())?;

    Ok(user)
  }

  #[graphql(guard(AuthenticationGuard()))]
  async fn delete_user(&self, ctx: &Context<'_>, id: Uuid) -> Result<bool> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let user_gateway = UserGatewayPostgres::new(&global_context.db_pool);

    let user = user_gateway
      .get(id)
      .await
      .map_err(|e| e.extend())?
      .ok_or_else(|| Error::NotFound(format!("user {}", id)).extend())?;

    user_gateway
      .delete(user)
      .await
      .map_err(|e| e.extend())?;

    Ok(true)
  }
}
