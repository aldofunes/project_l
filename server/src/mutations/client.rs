use crate::{
  error::Error,
  gateways::ClientGatewayPostgres,
  graphql_ctx,
  guards::{AuthenticationGuard, MembershipGuard},
  objects::Client,
  ports::ClientPort,
};
use async_graphql::{guard::Guard, Context, ErrorExtensions, Object, Result};
use chrono::Utc;
use uuid::Uuid;

#[derive(Default)]
pub struct ClientQuery;

#[derive(Default)]
pub struct ClientMutation;

#[Object]
impl ClientMutation {
  #[graphql(guard(chain(AuthenticationGuard(), MembershipGuard(organization_id = "@organization_id"))))]
  #[allow(clippy::too_many_arguments)]
  async fn register_client(
    &self,
    ctx: &Context<'_>,
    organization_id: Uuid,
    name: Option<String>,
    street_address: Option<String>,
    city: Option<String>,
    state: Option<String>,
    postal_code: Option<String>,
    country_code: Option<String>,
    phone_number: Option<String>,
    website_url: Option<String>,
  ) -> Result<Client> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let client_gateway = ClientGatewayPostgres::new(&global_context.db_pool);

    let client = client_gateway
      .insert(Client {
        id: Uuid::new_v4(),
        organization_id,
        registered_at: Utc::now().naive_utc(),
        name,
        street_address,
        city,
        state,
        postal_code,
        country_code,
        phone_number,
        website_url,
      })
      .await
      .map_err(|e| e.extend())?;

    Ok(client)
  }

  #[graphql(guard(AuthenticationGuard()))]
  #[allow(clippy::too_many_arguments)]
  async fn update_client(
    &self,
    ctx: &Context<'_>,
    id: Uuid,
    name: Option<String>,
    street_address: Option<String>,
    city: Option<String>,
    state: Option<String>,
    postal_code: Option<String>,
    country_code: Option<String>,
    phone_number: Option<String>,
    website_url: Option<String>,
  ) -> Result<Client> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let client_gateway = ClientGatewayPostgres::new(&global_context.db_pool);

    let mut client = client_gateway
      .get(id)
      .await
      .map_err(|e| e.extend())?
      .ok_or_else(|| Error::NotFound(format!("client {}", id)).extend())?;

    if name.is_some() {
      client.name = name;
    };
    if street_address.is_some() {
      client.street_address = street_address;
    };
    if city.is_some() {
      client.city = city;
    };
    if state.is_some() {
      client.state = state;
    };
    if postal_code.is_some() {
      client.postal_code = postal_code;
    };
    if country_code.is_some() {
      client.country_code = country_code;
    };
    if phone_number.is_some() {
      client.phone_number = phone_number;
    };
    if website_url.is_some() {
      client.website_url = website_url;
    };

    let client = client_gateway
      .update(client)
      .await
      .map_err(|e| e.extend())?;

    Ok(client)
  }

  #[graphql(guard(AuthenticationGuard()))]
  #[allow(clippy::too_many_arguments)]
  async fn delete_client(&self, ctx: &Context<'_>, id: Uuid) -> Result<bool> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let client_gateway = ClientGatewayPostgres::new(&global_context.db_pool);

    let client = client_gateway
      .get(id)
      .await
      .map_err(|e| e.extend())?
      .ok_or_else(|| Error::NotFound(format!("client {}", id)).extend())?;

    client_gateway
      .delete(client)
      .await
      .map_err(|e| e.extend())?;

    Ok(true)
  }
}
