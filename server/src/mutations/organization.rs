use crate::{
  gateways::OrganizationGatewayPostgres, graphql_ctx, guards::AuthenticationGuard, objects::Organization,
  ports::OrganizationPort,
};
use async_graphql::{guard::Guard, Context, ErrorExtensions, Object, Result};
use chrono::{NaiveDate, Utc};
use uuid::Uuid;

#[derive(Default)]
pub struct OrganizationMutation;

#[Object]
impl OrganizationMutation {
  #[graphql(guard(AuthenticationGuard()))]
  #[allow(clippy::too_many_arguments)]
  async fn register_organization(
    &self,
    ctx: &Context<'_>,
    name: String,
    slug: String,
    logo_url: Option<String>,
    street_address: Option<String>,
    city: Option<String>,
    state: Option<String>,
    postal_code: Option<String>,
    country_code: Option<String>,
    phone_number: Option<String>,
    website_url: Option<String>,
    employees: Option<i16>,
    founded_at: Option<NaiveDate>,
    annual_revenue_range: Option<String>,
  ) -> Result<Organization> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let organization_gateway = OrganizationGatewayPostgres::new(&global_context.db_pool);

    let organization = organization_gateway
      .insert(Organization {
        id: Uuid::new_v4(),
        registered_at: Utc::now().naive_utc(),
        name,
        slug,
        logo_url,
        street_address,
        city,
        state,
        postal_code,
        country_code,
        phone_number,
        website_url,
        employees,
        founded_at,
        annual_revenue_range,
      })
      .await
      .map_err(|e| e.extend())?;

    Ok(organization)
  }
}
