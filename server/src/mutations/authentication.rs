use crate::{
  error::Error,
  gateways::{OrganizationGatewayPostgres, UserGatewayPostgres},
  graphql_ctx,
  objects::{Organization, JwtClaims, User},
  ports::{OrganizationPort, UserPort},
  utils::build_jwt,
};
use ::http::header::SET_COOKIE;
use argon2::password_hash::{PasswordHash, PasswordHasher, PasswordVerifier, SaltString};
use async_graphql::{Context, ErrorExtensions, Object, Result, SimpleObject};
use chrono::{Duration, Utc};
use jsonwebtoken::{decode, Algorithm, DecodingKey, Validation};
use rand_core::OsRng;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

const ACCESS_TOKEN_TTL_MINUTES: i64 = 10;
const REFRESH_TOKEN_TTL_DAYS: i64 = 7;

#[derive(Default)]
pub struct AuthenticationMutation;

#[derive(Debug, Serialize, Deserialize, SimpleObject)]
pub struct SignUpResponse {
  organization: Organization,
  user: User,
  access_token: String,
}

#[Object]
impl AuthenticationMutation {
  async fn sign_in(&self, ctx: &Context<'_>, email: String, password: String) -> Result<String> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let user_gateway = UserGatewayPostgres::new(&global_context.db_pool);
    let organization_gateway = OrganizationGatewayPostgres::new(&global_context.db_pool);

    let user = user_gateway
      .get_by_email(email)
      .await
      .map_err(|e| e.extend())?
      .ok_or_else(|| Error::Unauthorized(String::from("invalid email")).extend())?;

    let parsed_hash = PasswordHash::new(&user.password_hash).unwrap();
    global_context
      .argon2
      .verify_password(password.as_bytes(), &parsed_hash)
      .map_err(|_err| Error::Unauthorized(String::from("invalid password")).extend())?;

    let organization_ids: Vec<Uuid> = organization_gateway
      .find_organizations_for_user(user.id)
      .await
      .map_err(|e| e.extend())?
      .iter()
      .map(|organization| organization.id)
      .collect();

    let access_token = build_jwt(
      &global_context.config.jwt_signing_key,
      &user.id,
      organization_ids.clone(),
      Duration::minutes(ACCESS_TOKEN_TTL_MINUTES),
    )?;

    let refresh_token = build_jwt(
      &global_context.config.jwt_signing_key,
      &user.id,
      organization_ids.clone(),
      Duration::days(REFRESH_TOKEN_TTL_DAYS),
    )?;

    ctx.append_http_header(
      SET_COOKIE,
      format!(
        "refresh_token={}; HttpOnly; Secure; Max-Age={}",
        refresh_token,
        Duration::days(REFRESH_TOKEN_TTL_DAYS).num_seconds()
      ),
    );

    Ok(access_token)
  }

  async fn sign_out(&self, ctx: &Context<'_>) -> bool {
    ctx.append_http_header("Clear-Site-Data", "\"cookies\"");

    true
  }

  async fn refresh_token(&self, ctx: &Context<'_>) -> Result<String> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;
    let request_context = ctx.data::<graphql_ctx::RequestContext>()?;

    let refresh_token = request_context
      .refresh_token
      .as_ref()
      .ok_or_else(|| Error::Unauthorized(String::from("refresh token not provided")).extend())?;

    let jwt_claims = decode::<JwtClaims>(
      refresh_token,
      &DecodingKey::from_secret(global_context.config.jwt_signing_key.as_bytes()),
      &Validation::new(Algorithm::HS256),
    )?;

    let user_id = jwt_claims.claims.sub;
    let user_gateway = UserGatewayPostgres::new(&global_context.db_pool);
    let user = user_gateway
      .get(user_id)
      .await
      .map_err(|e| e.extend())?
      .ok_or_else(|| Error::Unauthorized(String::from("user not found")).extend())?;

    let organization_gateway = OrganizationGatewayPostgres::new(&global_context.db_pool);
    let organization_ids: Vec<Uuid> = organization_gateway
      .find_organizations_for_user(user_id)
      .await
      .map_err(|e| e.extend())?
      .iter()
      .map(|organization| organization.id)
      .collect();

    let access_token = build_jwt(
      &global_context.config.jwt_signing_key,
      &user.id,
      organization_ids,
      Duration::minutes(ACCESS_TOKEN_TTL_MINUTES),
    )?;

    Ok(access_token)
  }

  #[allow(clippy::too_many_arguments)]
  async fn sign_up(
    &self,
    ctx: &Context<'_>,
    organization_name: String,
    email: String,
    password: String,
    first_name: String,
    last_name: String,
    time_zone: Option<String>,
    language: Option<String>,
  ) -> Result<SignUpResponse> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let organization_gateway = OrganizationGatewayPostgres::new(&global_context.db_pool);
    let user_gateway = UserGatewayPostgres::new(&global_context.db_pool);

    let organization_slug = slug::slugify(&organization_name);

    let organization = organization_gateway
      .insert(Organization {
        id: Uuid::new_v4(),
        registered_at: Utc::now().naive_utc(),
        name: organization_name,
        slug: organization_slug,
        logo_url: None,
        street_address: None,
        city: None,
        state: None,
        postal_code: None,
        country_code: None,
        phone_number: None,
        website_url: None,
        employees: None,
        founded_at: None,
        annual_revenue_range: None,
      })
      .await
      .map_err(|e| e.extend())?;

    let salt = SaltString::generate(&mut OsRng);

    log::trace!("hash password");
    let password_hash = global_context
      .argon2
      .hash_password(password.as_bytes(), salt.as_ref())?
      .to_string();

    if user_gateway
      .get_by_email(email.clone())
      .await
      .map_err(|e| e.extend())?
      .is_some()
    {
      return Err(Error::AlreadyExists(format!("email {}", email.clone())).extend());
    }

    let username = slug::slugify(&first_name);
    if user_gateway
      .get_by_username(username.clone())
      .await
      .map_err(|e| e.extend())?
      .is_some()
    {
      return Err(Error::AlreadyExists(format!("username {}", username.clone())).extend());
    }

    let user = user_gateway
      .insert(User {
        id: Uuid::new_v4(),
        registered_at: Utc::now().naive_utc(),
        email,
        username,
        password_hash,
        first_name: Some(first_name),
        last_name: Some(last_name),
        time_zone,
        language,
        totp_secret: None,
      })
      .await
      .map_err(|e| e.extend())?;

    organization_gateway
      .add_user_to_organization(&organization, &user)
      .await
      .map_err(|e| e.extend())?;

    let organization_ids = vec![organization.id];

    let access_token = build_jwt(
      &global_context.config.jwt_signing_key,
      &user.id,
      organization_ids.clone(),
      Duration::minutes(ACCESS_TOKEN_TTL_MINUTES),
    )?;

    let refresh_token = build_jwt(
      &global_context.config.jwt_signing_key,
      &user.id,
      organization_ids.clone(),
      Duration::days(REFRESH_TOKEN_TTL_DAYS),
    )?;

    ctx.append_http_header(
      SET_COOKIE,
      format!(
        "refresh_token={}; HttpOnly; Secure; Max-Age={}",
        refresh_token,
        Duration::days(REFRESH_TOKEN_TTL_DAYS).num_seconds()
      ),
    );

    Ok(SignUpResponse {
      organization,
      user,
      access_token,
    })
  }
}
