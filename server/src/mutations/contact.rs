use crate::{
  gateways::ContactGatewayPostgres, graphql_ctx, guards::AuthenticationGuard, objects::Contact,
  ports::ContactPort,
};
use async_graphql::{guard::Guard, Context, ErrorExtensions, Object, Result};
use chrono::Utc;
use uuid::Uuid;

#[derive(Default)]
pub struct ContactQuery;

#[derive(Default)]
pub struct ContactMutation;

#[Object]
impl ContactMutation {
  #[graphql(guard(AuthenticationGuard()))]
  #[allow(clippy::too_many_arguments)]
  async fn register_contact(
    &self,
    ctx: &Context<'_>,
    client_id: Uuid,
    name: String,
    email: Option<String>,
    phone_number: Option<String>,
  ) -> Result<Contact> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let contact_gateway = ContactGatewayPostgres::new(&global_context.db_pool);

    let contact = contact_gateway
      .insert(Contact {
        id: Uuid::new_v4(),
        client_id,
        registered_at: Utc::now().naive_utc(),
        name,
        email,
        phone_number,
      })
      .await
      .map_err(|e| e.extend())?;

    Ok(contact)
  }
}
