use crate::{error::Error, objects::JwtClaims};
use chrono::{Duration, Utc};
use uuid::Uuid;

pub fn build_jwt(
  jwt_signing_key: &str,
  user_id: &Uuid,
  organization_ids: Vec<Uuid>,
  ttl: Duration,
) -> Result<String, Error> {
  log::trace!("create jwt");

  let claims = JwtClaims {
    jti: Uuid::new_v4(),
    iss: String::from("project-l"),
    sub: *user_id,
    aud: String::from("graphql"),
    exp: (Utc::now() + ttl).timestamp() as u64,
    iat: Utc::now().timestamp() as u64,
    organization_ids,
  };

  let jwt = jsonwebtoken::encode(
    &jsonwebtoken::Header::default(),
    &claims,
    &jsonwebtoken::EncodingKey::from_secret(jwt_signing_key.as_bytes()),
  )?;

  Ok(jwt)
}
