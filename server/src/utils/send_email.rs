use crate::{
  objects::{User, UserParameter},
  error::Error,
  ports::UserPort,
};
use argon2::{
  password_hash::{PasswordHasher, SaltString},
  Argon2,
};
use rand_core::OsRng;
use uuid::Uuid;

pub struct SendEmailUseCase<'a> {
  mailer: &mut SmtpTransport,
}

impl<'a> SendEmailUseCase<'a> {
  pub fn new(argon2: &'a Argon2<'a>, user_gateway: &'a impl UserPort) -> Self {
    Self {
      argon2,
      user_gateway,
    }
  }

  pub async fn execute(
    &self,
    organization_id: Uuid,
    email: String,
    password: String,
    first_name: Option<String>,
    last_name: Option<String>,
  ) -> Result<User, Error> {
    // send an email
    let email = EmailBuilder::new()
      // Addresses can be specified by the tuple (email, alias)
      .to(self.to.clone())
      // ... or by an address only
      .from("no-reply@aldofunes.com")
      .subject(self.subject.clone())
      .text(self.text.clone())
      .html(self.html.clone())
      .build()
      .unwrap()
      .into();

    // Send the email
    let result = mailer.send(email);
  }
}
