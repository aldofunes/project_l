use crate::{error::Error, objects::Email};
use async_trait::async_trait;
use uuid::Uuid;

#[async_trait]
pub trait EmailPort: Sync + Send {
  async fn get(&self, id: Uuid) -> Result<Option<Email>, Error>;
  async fn insert(&self, email: Email) -> Result<Email, Error>;
  async fn update(&self, email: Email) -> Result<Email, Error>;
  async fn delete(&self, email: Email) -> Result<(), Error>;
}
