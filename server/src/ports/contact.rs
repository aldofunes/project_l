use crate::{error::Error, objects::Contact};
use async_trait::async_trait;
use uuid::Uuid;

#[async_trait]
pub trait ContactPort: Sync + Send {
  async fn find(&self, organization_id: Uuid) -> Result<Vec<Contact>, Error>;
  async fn get(&self, id: Uuid) -> Result<Option<Contact>, Error>;
  async fn insert(&self, contact: Contact) -> Result<Contact, Error>;
  async fn update(&self, contact: Contact) -> Result<Contact, Error>;
  async fn delete(&self, contact: Contact) -> Result<(), Error>;
}
