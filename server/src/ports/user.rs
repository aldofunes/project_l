use crate::{
  error::Error,
  objects::{Organization, User},
};
use async_trait::async_trait;
use uuid::Uuid;

#[async_trait]
pub trait UserPort: Sync + Send {
  async fn find(&self, organization_id: Uuid) -> Result<Vec<User>, Error>;
  async fn get(&self, id: Uuid) -> Result<Option<User>, Error>;
  async fn get_by_email(&self, email: String) -> Result<Option<User>, Error>;
  async fn get_by_username(&self, username: String) -> Result<Option<User>, Error>;
  async fn insert(&self, user: User) -> Result<User, Error>;
  async fn update(&self, user: User) -> Result<User, Error>;
  async fn delete(&self, user: User) -> Result<(), Error>;
  async fn add_organization_to_user(&self, user: &User, organization: &Organization) -> Result<(), Error>;
  async fn remove_organization_from_user(&self, user: &User, organization: &Organization) -> Result<(), Error>;
}
