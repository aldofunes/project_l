use crate::{error::Error, objects::Client};
use async_trait::async_trait;
use uuid::Uuid;

#[async_trait]
pub trait ClientPort: Sync + Send {
  async fn find(&self, organization_id: Uuid) -> Result<Vec<Client>, Error>;
  async fn get(&self, id: Uuid) -> Result<Option<Client>, Error>;
  async fn insert(&self, client: Client) -> Result<Client, Error>;
  async fn update(&self, client: Client) -> Result<Client, Error>;
  async fn delete(&self, client: Client) -> Result<(), Error>;
}
