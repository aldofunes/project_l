use crate::{
  error::Error,
  objects::{Organization, User},
};
use async_trait::async_trait;
use uuid::Uuid;

#[async_trait]
pub trait OrganizationPort: Sync + Send {
  async fn find(&self) -> Result<Vec<Organization>, Error>;
  async fn find_organizations_for_user(&self, user_id: Uuid) -> Result<Vec<Organization>, Error>;
  async fn get(&self, id: Uuid) -> Result<Option<Organization>, Error>;
  async fn get_by_slug(&self, slug: String) -> Result<Option<Organization>, Error>;
  async fn insert(&self, organization: Organization) -> Result<Organization, Error>;
  async fn update(&self, organization: Organization) -> Result<Organization, Error>;
  async fn delete(&self, organization: Organization) -> Result<(), Error>;
  async fn add_user_to_organization(&self, organization: &Organization, user: &User) -> Result<(), Error>;
  async fn remove_user_from_organization(&self, organization: &Organization, user: &User) -> Result<(), Error>;
}
