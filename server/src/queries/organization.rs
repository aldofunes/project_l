use crate::{
  error::Error,
  gateways::OrganizationGatewayPostgres,
  graphql_ctx,
  guards::{AuthenticationGuard, MembershipGuard},
  objects::Organization,
  ports::OrganizationPort,
};
use async_graphql::{guard::Guard, Context, ErrorExtensions, Object, Result};
use uuid::Uuid;

#[derive(Default)]
pub struct OrganizationQuery;

#[Object]
impl OrganizationQuery {
  #[graphql(guard(chain(AuthenticationGuard(), MembershipGuard(organization_id = "@id"))))]
  async fn get_organization(&self, ctx: &Context<'_>, id: Uuid) -> Result<Organization> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let organization_gateway = OrganizationGatewayPostgres::new(&global_context.db_pool);

    organization_gateway
      .get(id)
      .await
      .map_err(|e| e.extend())?
      .ok_or_else(|| Error::NotFound(String::from("organization")).extend())
  }

  #[graphql(guard(AuthenticationGuard()))]
  async fn get_organization_by_slug(&self, ctx: &Context<'_>, slug: String) -> Result<Organization> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let organization_gateway = OrganizationGatewayPostgres::new(&global_context.db_pool);

    organization_gateway
      .get_by_slug(slug)
      .await
      .map_err(|e| e.extend())?
      .ok_or_else(|| Error::NotFound(String::from("organization not found")).extend())
  }

  #[graphql(guard(AuthenticationGuard()))]
  async fn find_organizations(&self, ctx: &Context<'_>) -> Result<Vec<Organization>> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;
    let request_context = ctx.data::<graphql_ctx::RequestContext>()?;

    match request_context.user_id {
      Some(user_id) => {
        let organization_gateway = OrganizationGatewayPostgres::new(&global_context.db_pool);
        organization_gateway
          .find_organizations_for_user(user_id)
          .await
          .map_err(|e| e.extend())
      }
      None => Err(Error::NotFound(String::from("user id not provided")).extend()),
    }
  }
}
