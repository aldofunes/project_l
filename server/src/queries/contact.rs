use crate::{
  error::Error, gateways::ContactGatewayPostgres, graphql_ctx, guards::AuthenticationGuard,
  objects::Contact, ports::ContactPort,
};
use async_graphql::{guard::Guard, Context, ErrorExtensions, Object, Result};
use uuid::Uuid;

#[derive(Default)]
pub struct ContactQuery;

#[derive(Default)]
pub struct ContactMutation;

#[Object]
impl ContactQuery {
  #[graphql(guard(AuthenticationGuard()))]
  async fn find_contacts(&self, ctx: &Context<'_>, client_id: Uuid) -> Result<Vec<Contact>> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let contact_gateway = ContactGatewayPostgres::new(&global_context.db_pool);

    let contacts = contact_gateway
      .find(client_id)
      .await
      .map_err(|e| e.extend())?;

    Ok(contacts)
  }

  #[graphql(guard(AuthenticationGuard()))]
  async fn get_contact(&self, ctx: &Context<'_>, id: Uuid) -> Result<Contact> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let contact_gateway = ContactGatewayPostgres::new(&global_context.db_pool);

    contact_gateway
      .get(id)
      .await
      .map_err(|e| e.extend())?
      .ok_or_else(|| Error::NotFound(String::from("contact")).extend())
  }
}
