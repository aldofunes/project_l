use crate::{
  error::Error, gateways::ClientGatewayPostgres, graphql_ctx, guards::AuthenticationGuard,
  objects::Client, ports::ClientPort,
};
use async_graphql::{guard::Guard, Context, ErrorExtensions, Object, Result};
use uuid::Uuid;

#[derive(Default)]
pub struct ClientQuery;

#[derive(Default)]
pub struct ClientMutation;

#[Object]
impl ClientQuery {
  #[graphql(guard(AuthenticationGuard()))]
  async fn find_clients(&self, ctx: &Context<'_>, organization_id: Uuid) -> Result<Vec<Client>> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let client_gateway = ClientGatewayPostgres::new(&global_context.db_pool);

    let clients = client_gateway.find(organization_id).await.map_err(|e| e.extend())?;

    Ok(clients)
  }

  #[graphql(guard(AuthenticationGuard()))]
  async fn get_client(&self, ctx: &Context<'_>, id: Uuid) -> Result<Client> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let client_gateway = ClientGatewayPostgres::new(&global_context.db_pool);

    client_gateway
      .get(id)
      .await
      .map_err(|e| e.extend())?
      .ok_or_else(|| Error::NotFound(String::from("client")).extend())
  }
}
