use crate::{
  error::Error, gateways::OrganizationGatewayPostgres, graphql_ctx, objects::PublicOrganization, ports::OrganizationPort,
};
use async_graphql::{Context, ErrorExtensions, Object, Result};

#[derive(Default)]
pub struct PublicOrganizationQuery;

#[Object]
impl PublicOrganizationQuery {
  async fn get_public_organization(&self, ctx: &Context<'_>, slug: String) -> Result<PublicOrganization> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let organization_gateway = OrganizationGatewayPostgres::new(&global_context.db_pool);

    let organization = organization_gateway
      .get_by_slug(slug)
      .await
      .map_err(|e| e.extend())?
      .ok_or_else(|| Error::NotFound(String::from("organization not found")).extend())?;

    Ok(PublicOrganization {
      id: organization.id,
      slug: organization.slug,
      name: organization.name,
    })
  }
}
