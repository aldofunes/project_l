use crate::{
  error::Error,
  gateways::UserGatewayPostgres,
  graphql_ctx,
  guards::{AuthenticationGuard, MembershipGuard},
  objects::User,
  ports::UserPort,
};
use async_graphql::{guard::Guard, Context, ErrorExtensions, Object, Result};
use uuid::Uuid;

#[derive(Default)]
pub struct UserQuery;

#[derive(Default)]
pub struct UserMutation;

#[Object]
impl UserQuery {
  #[graphql(guard(chain(AuthenticationGuard(), MembershipGuard(organization_id = "@organization_id"))))]
  async fn find_users(&self, ctx: &Context<'_>, organization_id: Uuid) -> Result<Vec<User>> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let user_gateway = UserGatewayPostgres::new(&global_context.db_pool);

    let users = user_gateway.find(organization_id).await.map_err(|e| e.extend())?;

    Ok(users)
  }

  #[graphql(guard(AuthenticationGuard()))]
  async fn get_user(&self, ctx: &Context<'_>, id: Uuid) -> Result<User> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let user_gateway = UserGatewayPostgres::new(&global_context.db_pool);

    user_gateway
      .get(id)
      .await
      .map_err(|e| e.extend())?
      .ok_or_else(|| Error::NotFound(String::from("user")).extend())
  }
}
