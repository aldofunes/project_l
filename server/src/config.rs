use crate::error::Error;
use std::env;

pub struct Config {
  pub connection_uri: String,
  pub jwt_signing_key: String,
}

impl Config {
  pub fn new() -> Result<Self, Error> {
    log::trace!("building configuration");

    Ok(Self {
      connection_uri: env::var("DB_CONNECTION_URI")?,
      jwt_signing_key: env::var("JWT_SIGNING_KEY")?,
    })
  }

  pub fn get_connection_uri(&self) -> &String {
    &self.connection_uri
  }
}
