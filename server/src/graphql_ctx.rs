use crate::config::Config;
use argon2::Argon2;
use sqlx::PgPool;
use uuid::Uuid;

pub struct GlobalContext {
  pub config: Config,
  pub db_pool: PgPool,
  pub argon2: Argon2<'static>,
}

#[derive(Debug, Clone)]
pub struct RequestContext {
  pub access_token: Option<String>,
  pub refresh_token: Option<String>,
  pub user_id: Option<Uuid>,
  pub organization_ids: Option<Vec<Uuid>>,
}
