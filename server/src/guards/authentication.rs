use crate::{error::Error, graphql_ctx, objects::JwtClaims};
use async_graphql::{guard::Guard, Context, ErrorExtensions, Result};
use jsonwebtoken::{decode, Algorithm, DecodingKey, Validation};

pub struct AuthenticationGuard {}

#[async_trait::async_trait]
impl Guard for AuthenticationGuard {
  async fn check(&self, ctx: &Context<'_>) -> Result<()> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;
    let request_context = ctx.data::<graphql_ctx::RequestContext>()?;

    let token = request_context
      .access_token
      .as_ref()
      .ok_or_else(|| Error::Unauthorized(String::from("access token not provided")).extend())?;

    decode::<JwtClaims>(
      &token,
      &DecodingKey::from_secret(global_context.config.jwt_signing_key.as_bytes()),
      &Validation::new(Algorithm::HS256),
    )
    .map_err(|err| Error::Unauthorized(format!("invalid access token: {}", err)).extend())?;

    Ok(())
  }
}
