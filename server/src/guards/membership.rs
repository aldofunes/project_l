use crate::{error::Error, graphql_ctx};
use async_graphql::{guard::Guard, Context, ErrorExtensions, Result};
use uuid::Uuid;

pub struct MembershipGuard {
  pub organization_id: Uuid,
}

#[async_trait::async_trait]
impl Guard for MembershipGuard {
  async fn check(&self, ctx: &Context<'_>) -> Result<()> {
    let request_context = ctx.data::<graphql_ctx::RequestContext>()?;

    let is_allowed = request_context.user_id.is_some()
      && match &request_context.organization_ids {
        Some(organization_ids) => organization_ids.contains(&self.organization_id),
        None => false,
      };

    if is_allowed {
      Ok(())
    } else {
      Err(
        Error::Forbidden(format!(
          "user {:?} cannot interact with organization {}",
          request_context.user_id, self.organization_id
        ))
        .extend(),
      )
    }
  }
}
