use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, sqlx::FromRow)]
pub struct JwtClaims {
  pub jti: Uuid,
  pub iss: String,
  pub sub: Uuid,
  pub aud: String,
  pub exp: u64,
  pub iat: u64,
  pub organization_ids: Vec<Uuid>,
}
