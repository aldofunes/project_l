use async_graphql::SimpleObject;
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, SimpleObject, sqlx::FromRow)]
pub struct Document {
  pub id: Uuid,
  pub organization_id: Uuid,
  pub url: String,
  pub created_at: NaiveDateTime,
}
