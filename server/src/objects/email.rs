use async_graphql::SimpleObject;
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, SimpleObject, sqlx::FromRow)]
pub struct Email {
  pub id: Uuid,
  pub organization_id: Uuid,
  pub reason: Option<String>,
  pub from: Option<String>,
  pub to: String,
  pub subject: String,
  pub text: String,
  pub html: String,
  pub sent_at: NaiveDateTime,
}
