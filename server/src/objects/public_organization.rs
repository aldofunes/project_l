use async_graphql::SimpleObject;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, SimpleObject, sqlx::FromRow)]
pub struct PublicOrganization {
  /// system-generated uuid that uniquely identifies the organization
  pub id: Uuid,

  /// url-friendly unique identifier for a organization
  pub slug: String,

  /// full name of the organization
  pub name: String,
}
