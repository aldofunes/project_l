use crate::{gateways::ContactGatewayPostgres, graphql_ctx, objects::Contact, ports::ContactPort};
use async_graphql::{ComplexObject, Context, ErrorExtensions, Result, SimpleObject};
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, SimpleObject, sqlx::FromRow)]
#[graphql(complex)]
pub struct Client {
  pub id: Uuid,
  pub organization_id: Uuid,
  pub registered_at: NaiveDateTime,

  pub name: Option<String>,
  pub street_address: Option<String>,
  pub city: Option<String>,
  pub state: Option<String>,
  pub postal_code: Option<String>,
  pub country_code: Option<String>,
  pub phone_number: Option<String>,
  pub website_url: Option<String>,
}

#[ComplexObject]
impl Client {
  async fn contacts(&self, ctx: &Context<'_>) -> Result<Vec<Contact>> {
    let global_context = ctx.data::<graphql_ctx::GlobalContext>()?;

    let contact_gateway = ContactGatewayPostgres::new(&global_context.db_pool);

    contact_gateway.find(self.id).await.map_err(|e| e.extend())
  }
}
