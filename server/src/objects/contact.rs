use async_graphql::SimpleObject;
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

/// Represents a reachable contact
///
/// # Example
///
/// ```
/// let contact = Contact {
///   id: Uuid::new_v4(),
///   client_id: Uuid::new_v4(),
///   name: String::from("John Doe"),
///   email: Some(String::from("john.doe@example.com")),
///   phone_calling_code: Some(String::from("+52")),
///   phone_country_code: Some(String::from("MEX")),
///   phone: Some(String::From("5522889933")),
///   registered_at: Utc::now().naive_utc(),
/// };
/// ```
#[derive(Debug, Serialize, Deserialize, SimpleObject, sqlx::FromRow)]
pub struct Contact {
  pub id: Uuid,
  pub client_id: Uuid,
  pub registered_at: NaiveDateTime,

  /// the contact's full name
  pub name: String,

  /// the contact's email
  pub email: Option<String>,
  /// the contact's phone number
  pub phone_number: Option<String>,
}
