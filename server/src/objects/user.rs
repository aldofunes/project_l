use async_graphql::SimpleObject;
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, SimpleObject, sqlx::FromRow)]
pub struct User {
  pub id: Uuid,
  pub registered_at: NaiveDateTime,

  pub username: String,
  pub email: String,
  #[graphql(skip)]
  pub password_hash: String,

  pub first_name: Option<String>,
  pub last_name: Option<String>,
  pub time_zone: Option<String>,
  pub language: Option<String>,
  pub totp_secret: Option<String>,
}
