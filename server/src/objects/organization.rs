use async_graphql::SimpleObject;
use chrono::{NaiveDate, NaiveDateTime};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize, SimpleObject, sqlx::FromRow)]
pub struct Organization {
  /// system-generated uuid that uniquely identifies the organization
  pub id: Uuid,

  /// date and time of registration into the system
  pub registered_at: NaiveDateTime,

  /// url-friendly unique identifier for a organization
  pub slug: String,

  /// full name of the organization
  pub name: String,

  /// url to the image of the organization's logo
  pub logo_url: Option<String>,

  /// registered address
  pub street_address: Option<String>,

  /// registered address
  pub city: Option<String>,

  /// registered address
  pub state: Option<String>,

  /// registered address
  pub postal_code: Option<String>,

  /// registered address country code in ISO 3
  pub country_code: Option<String>,

  /// phone number where the organization can be reached by customers
  pub phone_number: Option<String>,

  /// url of the organization's website
  pub website_url: Option<String>,

  /// amount of employees the organization has
  pub employees: Option<i16>,

  /// date at which the organization was registered
  pub founded_at: Option<NaiveDate>,

  /// the annual revenue range of the organization
  pub annual_revenue_range: Option<String>,
}
