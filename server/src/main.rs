#![warn(clippy::all)]
mod config;
mod error;
mod gateways;
mod graphql_ctx;
mod guards;
mod mutations;
mod objects;
mod ports;
mod queries;
mod server;
mod utils;

use config::Config;
use sqlx::PgPool;

#[tokio::main]
async fn main() {
  env_logger::init();

  let config = Config::new().unwrap();

  log::trace!("opening db pool");
  let db_pool = PgPool::connect(config.get_connection_uri()).await.unwrap();

  log::trace!("running migrations");
  sqlx::migrate!().run(&db_pool).await.unwrap();

  server::start().await.unwrap();
}
