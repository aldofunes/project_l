#[derive(Debug, thiserror::Error)]
pub enum Error {
  #[error("item already exists")]
  AlreadyExists(String),

  #[error("failed to sign claims")]
  Jwt(#[from] jsonwebtoken::errors::Error),

  #[error("not found")]
  NotFound(String),

  #[error("failed to hash password")]
  PasswordHasher(argon2::password_hash::Error),

  #[error("serializing/deserializing json failed")]
  SerdeJson(#[from] serde_json::Error),

  #[error("sql command failed")]
  Sql(#[from] sqlx::Error),

  #[error("failed to run migrations")]
  SqlMigration(#[from] sqlx::migrate::MigrateError),

  #[error("unauthorized")]
  Unauthorized(String),

  #[error("you are not allowed to execute this action")]
  Forbidden(String),

  #[error("configuration error")]
  VarError(#[from] std::env::VarError),
}

impl From<argon2::password_hash::Error> for Error {
  fn from(error: argon2::password_hash::Error) -> Self {
    Error::PasswordHasher(error)
  }
}

impl async_graphql::ErrorExtensions for Error {
  // lets define our base extensions
  fn extend(&self) -> async_graphql::Error {
    async_graphql::Error::new(format!("{}", self)).extend_with(|_err, e| match self {
      Error::AlreadyExists(item) => {
        e.set("code", "ALREADY_EXISTS");
        e.set("item", item.to_string());
      }
      Error::NotFound(item) => {
        e.set("code", "NOT_FOUND");
        e.set("item", item.to_string());
      }
      Error::Unauthorized(reason) => {
        e.set("code", "UNAUTHORIZED");
        e.set("reason", reason.to_string());
      }
      Error::Jwt(reason) => {
        e.set("code", "UNAUTHORIZED");
        e.set("reason", reason.to_string());
      }
      Error::Forbidden(reason) => {
        e.set("code", "FORBIDDEN");
        e.set("reason", reason.to_string());
      }
      Error::Sql(error) => {
        e.set("code", "SQL_ERROR");
        e.set("reason", error.to_string());
      }
      _ => {}
    })
  }
}
